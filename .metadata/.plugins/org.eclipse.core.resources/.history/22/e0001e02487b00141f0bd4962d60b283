 /**
 	\file INIT-LCD.c
 	\brief Archivo con función de inicialización del LCD.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t Buffer_LCD[LCDBUFFER_SIZE];
	extern volatile uint32_t inxInLCD;
	extern volatile uint32_t inxOutLCD;
	extern volatile uint32_t cantidadColaLCD;
	extern volatile uint32_t Demora_LCD;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Init_LCD ( void )
	\brief Función de configuración de los pines del LCD.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Init_LCD ( void )
{
	SetPINSEL ( LCD_D4 , FUNCION_GPIO );	//LCD_D4
	SetPINSEL ( LCD_D5 , FUNCION_GPIO );	//LCD_D5
	SetPINSEL ( LCD_D6 , FUNCION_GPIO );	//LCD_D6
	SetPINSEL ( LCD_D7 , FUNCION_GPIO );	//LCD_D7
	SetPINSEL ( LCD_RS , FUNCION_GPIO );	//LCD_RS
	SetPINSEL ( LCD_E , FUNCION_GPIO );		//LCD_E

	SET_DIR(LCD_D4, SALIDA);
	SET_DIR(LCD_D5, SALIDA);
	SET_DIR(LCD_D6, SALIDA);
	SET_DIR(LCD_D7, SALIDA);
	SET_DIR(LCD_RS, SALIDA);
	SET_DIR(LCD_E, SALIDA);

	SET_PIN(LCD_D4,OFF);
	SET_PIN(LCD_D5,OFF);
	SET_PIN(LCD_D6,OFF);
	SET_PIN(LCD_D7,OFF);
	SET_PIN(LCD_E,OFF);
	SET_PIN(LCD_RS,OFF);

	uint8_t i;

	SET_PIN ( LCD_E , OFF );
	Demora_LCD = 10;				// Demora inicial
	while ( Demora_LCD );

	for( i = 0 ; i < 3 ; i++ )
	{
		SET_PIN(LCD_D4,ON);			// Configuracion en 8 bits
		SET_PIN(LCD_D5,ON);
		SET_PIN(LCD_D6,OFF);
		SET_PIN(LCD_D7,OFF);
		SET_PIN(LCD_RS,OFF);
		SET_PIN(LCD_E,ON);
		SET_PIN(LCD_E,OFF);
		Demora_LCD = 2;
		while ( Demora_LCD );
	}

	SET_PIN(LCD_D4,OFF);			// Configuracion en 4 bits
	SET_PIN(LCD_D5,ON);
	SET_PIN(LCD_D6,OFF);
	SET_PIN(LCD_D7,OFF);
	SET_PIN(LCD_RS,OFF);
	SET_PIN(LCD_E,ON);
	SET_PIN(LCD_E,OFF);

	Demora_LCD = 1;	// Demora inicial
	while (Demora_LCD);

	// A partir de aca el LCD pasa a 4 bits !!!
	PushLCD( 0x28 , LCD_CONTROL );	// DL = 0: 4 bits de datos
									// N = 1 : 2 lineas
									// F = 0 : 5x7 puntos

	PushLCD( 0x08 , LCD_CONTROL);	// D = 0 : display OFF
									// C = 0 : Cursor OFF
									// B = 0 : Blink OFF

	PushLCD( 0x01 , LCD_CONTROL);	// clear display

	PushLCD( 0x06 , LCD_CONTROL);	// I/D = 1 : Incrementa puntero
									// S = 0 : NO Shift Display

	// Activo el LCD y listo para usar !
	PushLCD( 0x0C , LCD_CONTROL);	// D = 1 : display ON
									// C = 0 : Cursor OFF
									// B = 0 : Blink OFF
}
