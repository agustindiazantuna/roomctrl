 /**
 	\file FW-DHT11.c
 	\brief Archivo que contiene el driver del sensor DHT11.
 	\details Contiene la función DHT_Fw().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************


// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void DHT_Fw ( void )
	\brief Driver del sensor DHT11.
 	\details Máquina de estados. IDLE: Coloca un estado bajo en el pin de comunicación. START: Tras esperar el tiempo mínimo del pulso en estado bajo, lo coloca en estado alto, cambia su configuración a captura y habilita las interrupciones. READING: Aguarda a que se termine de leer la trama de datos y configura el pin para la próxima lectura.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void DHT_Fw ( void )
{
	uint32_t aux=0;
	switch ( ESTADOS_FW_DHT11 )
	{
		case IDLE :
			if ( DHT_ON == 1 )
			{
				ESTADOS_FW_DHT11 = START;
				DHT_ON = 0;

				SET_PIN ( DHT , BAJO );

				DHT_CONT = 1;
			}
			break;
		case START :
			if ( DHT_CONT == 10 )
			{
				ESTADOS_FW_DHT11 = READING;
				DHT_CONT = 1;

				SET_PIN ( DHT , ALTO );

				T3CCR = 0x38;						// Habilita int de Captura para CAP3.1 por falling edge y rising edge

				SET_DIR ( DHT , ENTRADA );
				SetPINSEL ( DHT , FUNCION_3 );
			}
			break;
		case READING :
			if ( DHT_RECIVING >= 41 )									// SI SE RECIBIERON LOS 41 BITS
			{
				ESTADOS_FW_DHT11 = IDLE;

				BufferDHT_Tem = BufferDHT_Gral;
				BufferDHT_Tem = ( BufferDHT_Tem >> 8 );

//				BufferDHT_Hum = BufferDHT_Gral;

//				aux = (uint32_t)( (uint32_t)BufferDHT_Gral >> 1) ;				// NO SE COMO EXTRAERLOS SIN RECURRIR A COPIAR BIT A BIT

				aux = BufferDHT_Gral;
				BufferDHT_Hum = ( aux >> 16 );

//				BufferDHT_Hum = ( BufferDHT_Hum / 65536 );
				BufferDHT_Hum = ( BufferDHT_Hum / 256 );

				SetPINSEL ( DHT , FUNCION_GPIO );
				SET_DIR ( DHT , SALIDA );
				SET_PIN ( DHT , ALTO );

				DHT_RECIVING = 0;

				DHT_OK = 1;
			}
			else if ( DHT_CONT == 10 )
			{
				ESTADOS_FW_DHT11 = IDLE;
				BufferDHT_Gral = 0;
				DHT_ON = 1;
				DHT_CONT = 0;
			}
			break;
		default :
			break;
	}
}
