 /**
 	\file INIT-KIT.c
 	\brief Archivo con función de inicialización del Kit Infotronic.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Init_Kit ( void )
	\brief Función de configuración de los pines del Kit.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Init_Kit ( void )
{
	// TECLADO
	SetPINSEL ( SW1 , FUNCION_GPIO );
	SET_DIR ( SW1 , ENTRADA );

	SetPINSEL ( SW2 , FUNCION_GPIO );
	SET_DIR ( SW2 , ENTRADA );

	SetPINSEL ( SW3 , FUNCION_GPIO );
	SET_DIR ( SW3 , ENTRADA );

	SetPINSEL ( SW4 , FUNCION_GPIO );
	SET_DIR ( SW4 , ENTRADA );

//	SetPINSEL ( SW5 , FUNCION_GPIO );
//	SET_DIR ( SW5 , ENTRADA );

	// LEDs RGB:
	SetPINSEL ( RGBR , FUNCION_GPIO );
	SET_DIR ( RGBR , SALIDA );
	SET_PIN ( RGBR , ON );

	SetPINSEL ( RGBG , FUNCION_GPIO );
	SET_DIR ( RGBG , SALIDA );
	SET_PIN ( RGBG , ON );

	SetPINSEL ( RGBB , FUNCION_GPIO );
	SET_DIR ( RGBB , SALIDA );
	SET_PIN ( RGBB , ON );

	// BUZZER
	SetPINSEL ( BUZZ , FUNCION_GPIO );
	SET_DIR ( BUZZ , SALIDA );
	SET_PIN ( BUZZ , OFF );

	// RELAYS
	SetPINSEL ( RELAY0 , FUNCION_GPIO );
	SetPINSEL ( RELAY1 , FUNCION_GPIO );
	SetPINSEL ( RELAY2 , FUNCION_GPIO );
	SetPINSEL ( RELAY3 , FUNCION_GPIO );

	SET_DIR ( RELAY0 , SALIDA );
	SET_DIR ( RELAY1 , SALIDA );
	SET_DIR ( RELAY2 , SALIDA );
	SET_DIR ( RELAY3 , SALIDA );

	SET_PIN ( RELAY0 , ON );		// RELAY 1
	SET_PIN ( RELAY1 , ON );		// RELAY 2
	SET_PIN ( RELAY2 , ON );		// RELAY 0
	SET_PIN ( RELAY3 , ON );		// RELAY 3
}
