//*****************************************************************************
// LPC175x_6x Microcontroller Startup code for use with LPCXpresso IDE
//
// Version : 140114
//*****************************************************************************
//
// Copyright(C) NXP Semiconductors, 2014
// All rights reserved.
//
// Software that is described herein is for illustrative purposes only
// which provides customers with programming information regarding the
// LPC products.  This software is supplied "AS IS" without any warranties of
// any kind, and NXP Semiconductors and its licensor disclaim any and
// all warranties, express or implied, including all implied warranties of
// merchantability, fitness for a particular purpose and non-infringement of
// intellectual property rights.  NXP Semiconductors assumes no responsibility
// or liability for the use of the software, conveys no license or rights under any
// patent, copyright, mask work right, or any other intellectual property rights in
// or to any products. NXP Semiconductors reserves the right to make changes
// in the software without notification. NXP Semiconductors also makes no
// representation or warranty that such application will be suitable for the
// specified use without further testing or modification.
//
// Permission to use, copy, modify, and distribute this software and its
// documentation is hereby granted, under NXP Semiconductors' and its
// licensor's relevant copyrights in the software, without fee, provided that it
// is used in conjunction with NXP Semiconductors microcontrollers.  This
// copyright, permission, and disclaimer notice must appear in all copies of
// this code.
//*****************************************************************************

#include "Infotronic.h"

#if defined (__cplusplus)
#ifdef __REDLIB__
#error Redlib does not support C++
#else
//*****************************************************************************
//
// The entry point for the C++ library startup
//
//*****************************************************************************
extern "C" {
    extern void __libc_init_array(void);
}
#endif
#endif

#define WEAK __attribute__ ((weak))
#define ALIAS(f) __attribute__ ((weak, alias (#f)))

//*****************************************************************************
#if defined (__cplusplus)
extern "C" {
#endif

//*****************************************************************************
#if defined (__USE_CMSIS) || defined (__USE_LPCOPEN)
// Declaration of external SystemInit function
extern void SystemInit(void);
#endif

//*****************************************************************************
//
// Forward declaration of the default handlers. These are aliased.
// When the application defines a handler (with the same name), this will
// automatically take precedence over these weak definitions
//
//*****************************************************************************
     void ResetISR(void);
WEAK void NMI_Handler(void);
WEAK void HardFault_Handler(void);
WEAK void MemManage_Handler(void);
WEAK void BusFault_Handler(void);
WEAK void UsageFault_Handler(void);
WEAK void SVC_Handler(void);
WEAK void DebugMon_Handler(void);
WEAK void PendSV_Handler(void);
WEAK void SysTick_Handler(void);
WEAK void IntDefaultHandler(void);

//*****************************************************************************
//
// Forward declaration of the specific IRQ handlers. These are aliased
// to the IntDefaultHandler, which is a 'forever' loop. When the application
// defines a handler (with the same name), this will automatically take
// precedence over these weak definitions
//
//*****************************************************************************
void WDT_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER0_IRQHandler(void); // ALIAS(IntDefaultHandler);
void TIMER1_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER2_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER3_IRQHandler(void); // ALIAS(IntDefaultHandler);
void UART0_IRQHandler(void); // ALIAS(IntDefaultHandler);
void UART1_IRQHandler(void); // ALIAS(IntDefaultHandler);
void UART2_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART3_IRQHandler(void) ALIAS(IntDefaultHandler);
void PWM1_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C0_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C1_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C2_IRQHandler(void) ALIAS(IntDefaultHandler);
void SPI_IRQHandler(void) ALIAS(IntDefaultHandler);
void SSP0_IRQHandler(void) ALIAS(IntDefaultHandler);
void SSP1_IRQHandler(void) ALIAS(IntDefaultHandler);
void PLL0_IRQHandler(void) ALIAS(IntDefaultHandler);
void RTC_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT0_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT1_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT2_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT3_IRQHandler(void) ALIAS(IntDefaultHandler);
void ADC_IRQHandler(void) ALIAS(IntDefaultHandler);
void BOD_IRQHandler(void) ALIAS(IntDefaultHandler);
void USB_IRQHandler(void) ALIAS(IntDefaultHandler);
void CAN_IRQHandler(void) ALIAS(IntDefaultHandler);
void DMA_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2S_IRQHandler(void) ALIAS(IntDefaultHandler);
#if defined (__USE_LPCOPEN)
void ETH_IRQHandler(void) ALIAS(IntDefaultHandler);
#else
void ENET_IRQHandler(void) ALIAS(IntDefaultHandler);
#endif
void RIT_IRQHandler(void) ALIAS(IntDefaultHandler);
void MCPWM_IRQHandler(void) ALIAS(IntDefaultHandler);
void QEI_IRQHandler(void) ALIAS(IntDefaultHandler);
void PLL1_IRQHandler(void) ALIAS(IntDefaultHandler);
void USBActivity_IRQHandler(void) ALIAS(IntDefaultHandler);
void CANActivity_IRQHandler(void) ALIAS(IntDefaultHandler);

//*****************************************************************************
//
// The entry point for the application.
// __main() is the entry point for Redlib based applications
// main() is the entry point for Newlib based applications
//
//*****************************************************************************
#if defined (__REDLIB__)
extern void __main(void);
#endif
extern int main(void);
//*****************************************************************************
//
// External declaration for the pointer to the stack top from the Linker Script
//
//*****************************************************************************
extern void _vStackTop(void);

//*****************************************************************************
#if defined (__cplusplus)
} // extern "C"
#endif
//*****************************************************************************
//
// The vector table.
// This relies on the linker script to place at correct location in memory.
//
//*****************************************************************************
extern void (* const g_pfnVectors[])(void);
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) = {
    // Core Level - CM3
    &_vStackTop, // The initial stack pointer
    ResetISR,                               // The reset handler
    NMI_Handler,                            // The NMI handler
    HardFault_Handler,                      // The hard fault handler
    MemManage_Handler,                      // The MPU fault handler
    BusFault_Handler,                       // The bus fault handler
    UsageFault_Handler,                     // The usage fault handler
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    SVC_Handler,                            // SVCall handler
    DebugMon_Handler,                       // Debug monitor handler
    0,                                      // Reserved
    PendSV_Handler,                         // The PendSV handler
    SysTick_Handler,                        // The SysTick handler

    // Chip Level - LPC17
    WDT_IRQHandler,                         // 16, 0x40 - WDT
    TIMER0_IRQHandler,                      // 17, 0x44 - TIMER0
    TIMER1_IRQHandler,                      // 18, 0x48 - TIMER1
    TIMER2_IRQHandler,                      // 19, 0x4c - TIMER2
    TIMER3_IRQHandler,                      // 20, 0x50 - TIMER3
    UART0_IRQHandler,                       // 21, 0x54 - UART0
    UART1_IRQHandler,                       // 22, 0x58 - UART1
    UART2_IRQHandler,                       // 23, 0x5c - UART2
    UART3_IRQHandler,                       // 24, 0x60 - UART3
    PWM1_IRQHandler,                        // 25, 0x64 - PWM1
    I2C0_IRQHandler,                        // 26, 0x68 - I2C0
    I2C1_IRQHandler,                        // 27, 0x6c - I2C1
    I2C2_IRQHandler,                        // 28, 0x70 - I2C2
    SPI_IRQHandler,                         // 29, 0x74 - SPI
    SSP0_IRQHandler,                        // 30, 0x78 - SSP0
    SSP1_IRQHandler,                        // 31, 0x7c - SSP1
    PLL0_IRQHandler,                        // 32, 0x80 - PLL0 (Main PLL)
    RTC_IRQHandler,                         // 33, 0x84 - RTC
    EINT0_IRQHandler,                       // 34, 0x88 - EINT0
    EINT1_IRQHandler,                       // 35, 0x8c - EINT1
    EINT2_IRQHandler,                       // 36, 0x90 - EINT2
    EINT3_IRQHandler,                       // 37, 0x94 - EINT3
    ADC_IRQHandler,                         // 38, 0x98 - ADC
    BOD_IRQHandler,                         // 39, 0x9c - BOD
    USB_IRQHandler,                         // 40, 0xA0 - USB
    CAN_IRQHandler,                         // 41, 0xa4 - CAN
    DMA_IRQHandler,                         // 42, 0xa8 - GP DMA
    I2S_IRQHandler,                         // 43, 0xac - I2S
#if defined (__USE_LPCOPEN)
    ETH_IRQHandler,                         // 44, 0xb0 - Ethernet
#else
    ENET_IRQHandler,                        // 44, 0xb0 - Ethernet
#endif
    RIT_IRQHandler,                         // 45, 0xb4 - RITINT
    MCPWM_IRQHandler,                       // 46, 0xb8 - Motor Control PWM
    QEI_IRQHandler,                         // 47, 0xbc - Quadrature Encoder
    PLL1_IRQHandler,                        // 48, 0xc0 - PLL1 (USB PLL)
    USBActivity_IRQHandler,                 // 49, 0xc4 - USB Activity interrupt to wakeup
    CANActivity_IRQHandler,                 // 50, 0xc8 - CAN Activity interrupt to wakeup
};

//*****************************************************************************
// Functions to carry out the initialization of RW and BSS data sections. These
// are written as separate functions rather than being inlined within the
// ResetISR() function in order to cope with MCUs with multiple banks of
// memory.
//*****************************************************************************
__attribute__ ((section(".after_vectors")))
void data_init(unsigned int romstart, unsigned int start, unsigned int len) {
    unsigned int *pulDest = (unsigned int*) start;
    unsigned int *pulSrc = (unsigned int*) romstart;
    unsigned int loop;
    for (loop = 0; loop < len; loop = loop + 4)
        *pulDest++ = *pulSrc++;
}

__attribute__ ((section(".after_vectors")))
void bss_init(unsigned int start, unsigned int len) {
    unsigned int *pulDest = (unsigned int*) start;
    unsigned int loop;
    for (loop = 0; loop < len; loop = loop + 4)
        *pulDest++ = 0;
}

//*****************************************************************************
// The following symbols are constructs generated by the linker, indicating
// the location of various points in the "Global Section Table". This table is
// created by the linker via the Code Red managed linker script mechanism. It
// contains the load address, execution address and length of each RW data
// section and the execution and length of each BSS (zero initialized) section.
//*****************************************************************************
extern unsigned int __data_section_table;
extern unsigned int __data_section_table_end;
extern unsigned int __bss_section_table;
extern unsigned int __bss_section_table_end;

//*****************************************************************************
// Reset entry point for your code.
// Sets up a simple runtime environment and initializes the C/C++
// library.
//*****************************************************************************
__attribute__ ((section(".after_vectors")))
void
ResetISR(void) {

    //
    // Copy the data sections from flash to SRAM.
    //
    unsigned int LoadAddr, ExeAddr, SectionLen;
    unsigned int *SectionTableAddr;

    // Load base address of Global Section Table
    SectionTableAddr = &__data_section_table;

    // Copy the data sections from flash to SRAM.
    while (SectionTableAddr < &__data_section_table_end) {
        LoadAddr = *SectionTableAddr++;
        ExeAddr = *SectionTableAddr++;
        SectionLen = *SectionTableAddr++;
        data_init(LoadAddr, ExeAddr, SectionLen);
    }
    // At this point, SectionTableAddr = &__bss_section_table;
    // Zero fill the bss segment
    while (SectionTableAddr < &__bss_section_table_end) {
        ExeAddr = *SectionTableAddr++;
        SectionLen = *SectionTableAddr++;
        bss_init(ExeAddr, SectionLen);
    }

#if defined (__USE_CMSIS) || defined (__USE_LPCOPEN)
    SystemInit();
#endif

#if defined (__cplusplus)
    //
    // Call C++ library initialisation
    //
    __libc_init_array();
#endif

#if defined (__REDLIB__)
    // Call the Redlib library, which in turn calls main()
    __main() ;
#else
    main();
#endif

    //
    // main() shouldn't return, but if it does, we'll just enter an infinite loop
    //
    while (1) {
        ;
    }
}

//*****************************************************************************
// Default exception handlers. Override the ones here by defining your own
// handler routines in your application code.
//*****************************************************************************
__attribute__ ((section(".after_vectors")))
void NMI_Handler(void)
{ while(1) {}
}

__attribute__ ((section(".after_vectors")))
void HardFault_Handler(void)
 { while(1) {}
}

__attribute__ ((section(".after_vectors")))
void MemManage_Handler(void)
{ while(1) {}
}

__attribute__ ((section(".after_vectors")))
void BusFault_Handler(void)
{ while(1) {}
}

__attribute__ ((section(".after_vectors")))
void UsageFault_Handler(void)
{ while(1) {}
}

__attribute__ ((section(".after_vectors")))
void SVC_Handler(void)
{ while(1) {}
}

__attribute__ ((section(".after_vectors")))
void DebugMon_Handler(void)
{ while(1) {}
}

__attribute__ ((section(".after_vectors")))
void PendSV_Handler(void)
{ while(1) {}
}


// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	// DISPLAY
	extern unsigned char Digito_Display_E2[];
	// UART1
	extern volatile uint8_t TX_ENVIANDO;
	// LCD
	extern volatile uint32_t Demora_LCD;
	// DHT11
	extern volatile uint32_t dht_start;
	extern volatile uint8_t DHT_ON;
	extern volatile uint8_t DHT_CONT;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void TX_Send ( void )
	\brief Función principal del proyecto.
 	\details Contiene las funciones de inicialización del hardware y de control del mismo.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

__attribute__ ((section(".after_vectors")))
void SysTick_Handler(void)
{

	BarridoDisplay_E3 ( );
	DriverTeclado_E3 ( );

	// TECLADO KIT
	Debounce_KIT ( );
	//Debounce_ED_KIT ( );

	// LCD
	Dato_LCD();
	if ( Demora_LCD )
		Demora_LCD--;

	// HC-SR04
	HCSR04_Fw ( );

	// DHT
	if( dht_start > 0 )						// DEMORA EN LA INICIALIZACION
		dht_start --;
	if( dht_start == 0 )					// DRIVER DEL SENSOR		// ver por qué le puse esa condición
		DHT_Fw ( );
	if( DHT_CONT > 0 )						// CONTADOR DEL DRIVER
		DHT_CONT++;

	// DOOR
	Relay ( );

	// TIMERS
	Driver_Timers ( );

}

//*****************************************************************************
//
// Processor ends up here if an unexpected interrupt occurs or a specific
// handler is not present in the application code.
//
//*****************************************************************************
__attribute__ ((section(".after_vectors")))
void IntDefaultHandler(void)
{ while(1) {}
}

// ---------------------------------------------------------UART1--------------------------------------------------------- //

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile char TxStart;									//!< flag de fin de TX

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void TX_Send ( void )
	\brief Función principal del proyecto.
 	\details Contiene las funciones de inicialización del hardware y de control del mismo.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void UART1_IRQHandler(void)
{
	unsigned char iir,lsr;
	unsigned char IntEnCurso , IntPendiente ;
	int dato ;

	do
	{  //IIR es reset por HW, una vez que lo lei se resetea.
		iir = U1IIR;

		IntPendiente = iir & 0x01;
		IntEnCurso = ( iir >> 1 ) & 0x07;
		IntEnCurso = ( iir >> 1 ) & 0x07;
		IntEnCurso = ( iir >> 1 ) & 0x07;

		switch( IntEnCurso )
		{
			case IIR_RLS:
				lsr = U1LSR;						// con la lectura se borra la int
				if ( lsr & ( LSR_OE | LSR_PE | LSR_FE | LSR_RXFE|LSR_BI ) )
				{
					dato = U1RBR;					// Leo para borrar el buffer
					return;
				}
				break;
			case IIR_RDA:	// Receive Data Available
				U1LSR &=~ LSR_RDR;					// Borrar flag de Rx
				dato = U1RBR;						// Lectura del dato
				PushRx( (unsigned char ) dato );	// Guardo el dato
				break;

			case IIR_THRE:	// THRE, transmit holding register empty
				U1LSR &= ~LSR_THRE;					// Borrar flag de Tx
				dato = PopTx( );					// Tomo el dato a Transmitir
				if ( dato >= 0 )
					U1THR = (unsigned char) dato;	// si hay dato en la cola lo Transmito
				else
					TxStart = 0;
				break;
		}
	}
	while( ! ( IntPendiente & 0x01 ) ); // me fijo si cuando entr‚ a la ISR habia otra
						     			//int. pendiente de atenci¢n: b0=1 (ocurre £nicamente si dentro del mismo
										//espacio temporal lleguan dos interrupciones a la vez)
}


// -----------------------------------------------------TIMER3(DHT11)----------------------------------------------------- //

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t DHT_RECIVING;

	extern volatile uint32_t BufferDHT_Gral;
	extern volatile uint8_t BufferDHT_Par;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void TX_Send ( void )
	\brief Función principal del proyecto.
 	\details Contiene las funciones de inicialización del hardware y de control del mismo.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void TIMER3_IRQHandler ( void )
{
    if( ( T3IR & ( 1 << IRCR1 ) ) )			// Si está activo el flag de CAPTURE
    {
		T3IR = ( 1 << IRCR1 );				// Borro flag del CAPTURE

		if ( GET_PIN ( DHT , ALTO ) )		// Si fue por rising edge
		{
			T3TCR |= (0x01 << 1);			// Reseteo el temporizador
			T3TCR &= ~(0x01 << 1);			// Saco reset del temporizador
			T3TCR |= 0x01;					// Enciendo el temporizador
		}

		else
		{
			if ( DHT_RECIVING > 1 )
			{
				int t = ( T3CR1 * 4 ) / 100;

				if( DHT_RECIVING < 34 )
				{
					if ( t > 50 )
						BufferDHT_Gral |= ( 0x01 << ( 33 - DHT_RECIVING ) );
				}

				if ( DHT_RECIVING > 42 )
						T3CCR = 0x00;						// Deshabilita interrupciones
			}
			else
			{
				BufferDHT_Gral = 0;
			}

			DHT_RECIVING ++;

			T3TCR &= ~(0x01 << 0);						// Apago el temporizador
			T3TCR |= (0x01 << 1);						// Reseteo el temporizador
			T3TCR &= ~(0x01 << 1);						// Saco reset del temporizador
		}
    }
}

// ----------------------------------------------------TIMER0(HCSR04)---------------------------------------------------- //

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t RE_2;
	extern volatile uint32_t LEN;
	extern volatile uint8_t HCSR04_RECIVING;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void TX_Send ( void )
	\brief Función principal del proyecto.
 	\details Contiene las funciones de inicialización del hardware y de control del mismo.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void TIMER0_IRQHandler(void)
{
	if( ( T0IR & ( 1 << IRCR0 ) ) && RE_2 )	// Si interrumpio CAPTURE
	{
		T0TCR = 0x01;						// Enciendo el temporizador
		T0IR = ( 1 << IRCR0 );				// Borro flag del CAPTURE
		RE_2 = 0;
	}

	if( ( T0IR & ( 1 << IRCR0 ) ) && ~RE_2 )// Si interrumpio CAPTURE
	{
		T0IR = ( 1 << IRCR0 ); 				// Borro flag del CAPTURE

		LEN = ( T0CR0 * 4 ) / 100;
		LEN = LEN / 58;

		T0TCR = 0x00;						// Apago el temporizador
		T0TCR = 0x02;						// Reseteo el temporizador
		RE_2 = 1;

		HCSR04_RECIVING = 1;
	}
}

