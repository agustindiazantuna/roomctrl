 /**
 	\file INIT-DHT11.c
 	\brief Archivo con la inicialización del sensor DHT11.
 	\details Contiene la función de inicialización.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint32_t dht_start;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void InitDHT11 ( void )
	\brief Función inicialización del sensor DHT11.
 	\details Pin DATA del sensor es salida GPIO. Seteado en estado alto.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void InitDHT11 ( void )
{
	while ( dht_start );					// Bloqueante, pierde 1 segundo

	SetPINSEL ( DHT , FUNCION_GPIO );
	SET_DIR ( DHT , SALIDA );
	SET_PIN ( DHT , ALTO );
}
