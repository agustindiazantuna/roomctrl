 /**
 	\file RoomCTRL.c
 	\brief Aplicación principal.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void RoomCTRL ( void )
	\brief Función de control del proyecto.
 	\details Contiene las funciones de cada parte de RoomCTRL.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void RoomCTRL ( void )
{
	uint8_t button = NO_KEY;

	button = Tecla_E3 ( );					// Lee el teclado.

	// MÁQUINA DE TIMERS
	TimerEvent ( );

	// SENSORES
	DHT_App ( button );						// Humedad y temperatura.
	HCSR04_App ( );							// Ultrasonido.
	App_RDM6300 ( button );					// RFID.

	// DOOR
	DOOR_App ( button );					// Control de la placa.

	// UART
	Transmitir ( );
	Mensajes ( );
}
