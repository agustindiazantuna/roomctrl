 /**
 	\file APP-DHT11.c
 	\brief Aplicación del sensor de humedad y temperatura DHT11.
 	\details Contiene la función DHT_App().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t humidity;
	extern volatile uint8_t temperature;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void DHT11_App ( uint8_t button )
	\brief Función aplicación del sensor DHT11.
 	\details Utiliza la primitiva DHT_Prim() para obtener los valores leídos y procesados del sensor y mostrarlos en los display de 7 segmentos. Dispara un timer de 5 minutos (tiempo entre cada lectura).
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param button : Tipo uint8_t, contiene el valor del botón presionado.
	\return void
*/

void DHT_App ( uint8_t button )
{
	static uint8_t Hum = 0, Tem = 0, ret = 0, unidad = CELSIUS;

	ret = DHT_Prim ( &Hum , &Tem );

	if ( ret )				// Solo devuelve 1 cuando leyo
	{
		Display_E3 ( Hum , DSP0 );
		TimerStart( E_T1 , T_T1 , SEGUNDOS );
		humidity = Hum;
		temperature = Tem;
	}

	if ( button == 4 || button == 5 || button == 6 )
		unidad = button;

	switch ( unidad )
	{
		case CELSIUS :
			Display_E3 ( Tem , DSP1 );
			break;
		case KELVIN :
			Display_E3 ( ( Tem + 273 ) , DSP1 );
			break;
		case FAHRENHEIT :
			Display_E3 ( ( ( ( Tem * 9 ) / 5 ) + 32 ) , DSP1 );
			break;
	}

}
