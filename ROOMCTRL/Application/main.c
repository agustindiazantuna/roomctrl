 /**
 	\file main.c
 	\brief Main del proyecto RoomCTRL.
 	\details Programa principal.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	// VARIABLES PARA TRANSMICIÓN
	volatile uint8_t user_ok = 0;
	volatile uint8_t transmit_humidity = 0;
	volatile uint8_t humidity = 0;
	volatile uint8_t transmit_temperature = 0;
	volatile uint8_t temperature = 0;
	volatile uint8_t transmit_camera = 0;
	volatile uint8_t camera = 0;
	volatile uint8_t transmit_id = 0;
	volatile uint8_t mifare = 0;
	volatile uint8_t transmit_door;
	// VARIABLES RECIBIDAS
	volatile uint8_t result_mifare_id = 0;
	//TIMERS
	volatile timers_t Timers[ C_TIMERS ];
	volatile uint8_t BufferSalidas;
	volatile uint8_t	decimas = DECIMAS;
	volatile uint8_t segundos = SEGUNDOS;
	volatile uint8_t minutos = MINUTOS;
	//KIT
	volatile uint8_t Buff_ED_KIT;
	volatile uint8_t Buff_TECLADO_KIT = NO_KEY;
	volatile uint8_t BufferTeclado_E3;
	//EXP3
	uint8_t Digito_Display_E3[6]={0xff,0xff,0xff,0xff,0xff,0xff};
	volatile uint8_t BufferTeclado_E3;
	//UART1
	volatile unsigned char BufferTx[TOPE_BUFFER_TX];		//!< Buffer de Transmision
	volatile unsigned char BufferRx[TOPE_BUFFER_RX];		//!< Buffer de Recepcion
	volatile unsigned char IndiceTxIn,IndiceTxOut;			//!< Indices de Transmision
	volatile unsigned char IndiceRxIn,IndiceRxOut;			//!< Indices de Recepcion
	volatile char TxStart;									//!< flag de fin de TX
	//UART0
	volatile unsigned char BufferTx0[TOPE_BUFFER_TX0];		//!< Buffer de Transmision
	volatile unsigned char BufferRx0[TOPE_BUFFER_RX0];		//!< Buffer de Recepcion
	volatile unsigned char IndiceTxIn0,IndiceTxOut0;		//!< Indices de Transmision
	volatile unsigned char IndiceRxIn0,IndiceRxOut0;		//!< Indices de Recepcion
	volatile char TxStart0;									//!< flag de fin de TX
	volatile uint8_t BufferSalidas0;
	volatile uint8_t rdm6300_on = 0;
	int FlagTrama;
	//---------------------------------------------------DHT11---------------------------------------------
	volatile uint32_t dht_start = 500;
	volatile uint8_t dht_on = 0;
	volatile uint8_t dht_cont = 0;
	volatile uint8_t dht_reciving = 0;
	volatile uint8_t dht_ok = 0;
	volatile uint32_t BufferDHT_Gral = 0;
	volatile uint16_t BufferDHT_Hum = 0;
	volatile uint16_t BufferDHT_Tem = 0;
	//---------------------------------------------------HCSR04---------------------------------------------
	volatile uint8_t hcsr04_on = 0;
	volatile uint8_t hcsr04_reciving = 0;
	volatile uint8_t hcsr04_ok = 0;
	volatile uint8_t hcsr04_expired;
	volatile uint32_t BufferHCSR04 = 0;
	volatile uint32_t len = 0;
	volatile uint8_t rfid_validate = 0;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn int main ( void )
	\brief Función principal del proyecto.
 	\details Contiene las funciones de inicialización del hardware y de control del mismo.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return Devuelve 0 si no hubo error.
*/

int main ( void )
{
	Inicializar ( );

    while ( 1 )
    {
    	RoomCTRL ( );
    }

    return 0 ;
}
