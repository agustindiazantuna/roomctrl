 /**
 	\file APP-HCSR04.c
 	\brief Aplicación del sensor de ultrasonido.
 	\details Contiene la función HCSR04_App().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t hcsr04_on;
	extern volatile uint8_t hcsr04_expired;
	extern volatile uint8_t rfid_validate;
	extern volatile uint8_t transmit_camera;
	extern volatile uint8_t camera;
	extern volatile uint8_t rdm6300_on;

	volatile uint8_t buzzer_detection;
	extern volatile uint8_t user_ok;

	volatile uint8_t led_green = 0;


// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void HCSR04_App ( void )
	\brief Función aplicación del sensor HCSR04.
 	\details Máquina de tres estados, WAITING: Espera a la detección de un estado y modifica flag para transmición de la cámara. MEASURING: Espera a tener 2 mediciones correctas, para cambiar de estado. VALIDATING: Con un timer le da tiempo a la persona para que ingrese abra la puerta en base al resultado de la lectura del RFID.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void HCSR04_App ( void )
{
	static uint32_t Dis = 0, variable_validating = 1;
	static uint32_t last_dis = 0;
	static uint8_t i = 0 , j = 0, ESTADOS_HCSR04 = WAITING;

	switch( ESTADOS_HCSR04 )
	{
		case WAITING:
			if ( HCSR04_Prim ( &Dis ) )					// Solo devuelve 1 cuando salio OK
			{
				if( Dis >= 1 && Dis <= 250 )
				{
					ESTADOS_HCSR04 = MEASURING;
					transmit_camera = 1;
					camera = 1;
				}
			}
			break;
		case MEASURING:
			if ( HCSR04_Prim ( &Dis ) )					// Solo devuelve 1 cuando salio OK
			{
				if( i == 0 )
					last_dis = Dis;

				if( Dis <= last_dis )
				{
					last_dis = Dis;
					j++;
				}

				i++;

				if( i == 20 || j == 2 )
				{
					if ( j == 2 )
						ESTADOS_HCSR04 = VALIDATING;
					else
						ESTADOS_HCSR04 = WAITING;
					i = 0;
					j = 0;
				}
			}
			break;
		case VALIDATING:
			if( last_dis <= 300 )
			{
				if ( variable_validating )
				{
					rdm6300_on = 1;
					hcsr04_on = 0;
					DisplayLCD ( "Acerque tarjeta" , RENGLON_1 , 0 );
					DisplayLCD ( "                " , RENGLON_2 , 0 );
					TimerStart( E_T2, T_T2 , SEGUNDOS );
					variable_validating = 0;
				}

				if( hcsr04_expired == 1 && rfid_validate == 0 )
				{
					ESTADOS_HCSR04 = WAITING;
					rdm6300_on = 0;
					transmit_camera = 2;
					camera = 2;
					hcsr04_expired = 0;

					variable_validating = 1;

					DisplayLCD( "    RoomCTRL    " , RENGLON_1 , 0 );
					DisplayLCD( "  Ready to use  " , RENGLON_2 , 0 );

					TimerStart( E_T5, 5 , SEGUNDOS );
				}

				else if( hcsr04_expired == 0 && rfid_validate == 1 )
				{
					TimerStop( E_T2 );

					hcsr04_expired = 0;

					led_green = 1;

					user_ok = rfid_validate;
					rfid_validate = 0;

					transmit_camera = 2;
					camera = 2;

					ESTADOS_HCSR04 = WAITING;

					TimerStart ( E_T3 , T_T3 , SEGUNDOS );		// El usuario tiene 45 segundos para abrir y cerrar la puerta.

					variable_validating = 1;
				}
			}
			else
				ESTADOS_HCSR04 = WAITING;
			break;
	}
}
