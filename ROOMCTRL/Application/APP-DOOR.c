 /**
 	\file APP-DOOR.c
 	\brief Aplicación que maneja el estado de las puertas.
 	\details Contiene la función DOOR_App().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	volatile uint8_t relays = 0;
	extern volatile uint8_t transmit_door;
	extern volatile uint8_t user_ok;
	extern volatile uint8_t led_green;


// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void DOOR_App ( uint8_t button )
	\brief Función aplicación del manejo de la puerta.
 	\details Modifica el buffer del estado de los relays en base al resultado del RFID.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param button : Tipo uint8_t, contiene el valor del botón presionado.
	\return void
*/

void DOOR_App ( uint8_t button )
{
	static uint8_t roomocupate = 0, dooropen = 0, cont = 0;

	if ( user_ok == 1 || dooropen == 1 || roomocupate == 1 )
	{
		if ( button == 3 && !dooropen )
		{
			user_ok = 0;
			dooropen = 1;

			TimerStop( E_T3 );

			relays = 1;

			if ( cont == 0 )
				transmit_door = 1;
		}
		else if ( button == 7 && dooropen )
		{
			cont ++;
			roomocupate = 1;
			dooropen = 0;

			led_green = 0;

			relays = 0;

			if ( cont == 1 )
			{
				DisplayLCD ( "   HABITACION   " , RENGLON_1 , 0 );
				DisplayLCD ( "    OCUPADA     " , RENGLON_2 , 0 );
			}

			if ( cont == 2 )
			{
				cont = 0;
				dooropen = 0;
				roomocupate = 0;

				TimerStart ( E_T4 , T_T4 , SEGUNDOS );

				DisplayLCD ( "   HABITACION   " , RENGLON_1 , 0 );
				DisplayLCD ( "     VACIA      " , RENGLON_2 , 0 );
			}
		}
	}
}
