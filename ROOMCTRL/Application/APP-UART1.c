 /**
 	\file UART1.c
 	\brief Aplicaciones para el uso del periférico UART.
 	\details Contiene una función dedicada a la transmición y otra a la recepción.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"
	#include "UART0.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t transmit_humidity;
	extern volatile uint8_t transmit_temperature;
	extern volatile uint8_t transmit_camera;
	extern volatile uint8_t transmit_id;
	volatile uint8_t transmit_new_id;
	extern volatile uint8_t transmit_door;

	extern volatile unsigned char BufferRx0[TOPE_BUFFER_RX0];

	extern volatile uint8_t humidity;
	extern volatile uint8_t temperature;
	extern volatile uint8_t camera;
	extern volatile uint8_t mifare;
	extern volatile uint8_t Trama[SIZE_BUFF_SERIE];


// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Transmitir ( void )
	\brief Función aplicación encargada de la transmición a través de la UART1 del LPC1769.
 	\details Mediante el uso del condicional if evalua el valor de ciertas variables globales (de la forma transmit_x), arma una trama específica y la cual se la pasa como parámetro a la función PushTx() para llenar el buffer de transmición.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Transmitir ( void )
{
	uint32_t i = 0;

	if ( transmit_humidity )			// Transmición de la humedad.
	{
		uint8_t h[5];
		h[0] = '$';
		h[1] = 'h';						// Humedad
		h[2] = ( humidity/10 + '0' );
		h[3] = ( humidity%10 + '0' );
		h[4] = '#';
		for ( i = 0 ; i < 5 ; i++ )
			PushTx ( h[i] );
		transmit_humidity = 0;
	}

	if ( transmit_temperature )			// Transmición de la temperatura.
	{
		uint8_t t[5];
		t[0] = '$';
		t[0] = '$';
		t[0] = '$';
		t[0] = '$';
		t[0] = '$';
		t[1] = 't';						// Temperatura
		t[2] = ( temperature/10 + '0' );
		t[3] = ( temperature%10 + '0' );
		t[4] = '#';
		for ( i = 0 ; i < 5 ; i++ )
			PushTx ( t[i] );
		transmit_temperature = 0;
	}

	if ( transmit_camera )				// Transmición de la activación de la cámara.
	{
		uint8_t c[4];
		c[0] = '$';
		c[1] = 'c';						// Camera
		c[2] = ( camera + '0' );		// Camera vale 1 si es la primer foto y 2 si es la segunda. Eso se modifica en APP-HCSR04.
		c[3] = '#';
		for ( i = 0 ; i < 4 ; i++ )
			PushTx( c[i] );
		transmit_camera = 0;
	}

	if ( transmit_id )			// Transmición del ID leído por MFRC522.
	{
		uint8_t m[13];
		m[0] = '$';
		m[1] = 'r';						// RFID
		m[2] = ( Trama[1] );
		m[3] = ( Trama[2] );
		m[4] = ( Trama[3] );
		m[5] = ( Trama[4] );
		m[6] = ( Trama[5] );
		m[7] = ( Trama[6] );
		m[8] = ( Trama[7] );
		m[9] = ( Trama[8] );
		m[10] = ( Trama[9] );
		m[11] = ( Trama[10] );
		m[12] = '#';
		for ( i = 0 ; i < 13 ; i++ )
			PushTx ( m[i] );
		for ( i = 0 ; i < SIZE_BUFF_SERIE ; i++ )
			Trama[i] = 0;
		transmit_id = 0;
	}

	if ( transmit_new_id )			// Transmición del ID leído por MFRC522.
	{
		uint8_t n[13];
		n[0] = '$';
		n[1] = 'n';						// Mifare
		n[2] = ( Trama[1] );
		n[3] = ( Trama[2] );
		n[4] = ( Trama[3] );
		n[5] = ( Trama[4] );
		n[6] = ( Trama[5] );
		n[7] = ( Trama[6] );
		n[8] = ( Trama[7] );
		n[9] = ( Trama[8] );
		n[10] = ( Trama[9] );
		n[11] = ( Trama[10] );
		n[12] = '#';
		for ( i = 0 ; i < 13 ; i++ )
			PushTx ( n[i] );
		for ( i = 0 ; i < SIZE_BUFF_SERIE ; i++ )
			Trama[i] = 0;
		transmit_new_id = 0;
	}

	if ( transmit_door )			// Transmición del estado de la puerta.
	{
		uint8_t d[4];
		d[0] = '$';
		d[1] = 'd';							// Door.
		d[2] = ( transmit_door + '0' );
		d[3] = '#';
		for ( i = 0 ; i < 4 ; i++ )
			PushTx ( d[i] );
		transmit_door = 0;
	}
}

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t rfid_validate;
	extern volatile unsigned char BufferRx[TOPE_BUFFER_RX];

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Mensajes ( void )
	\brief Función aplicación encargada de la recepción a través de la UART1 del LPC1769.
 	\details Mediante el uso de la función PopRx lee el buffer de lo que se ha recibido y lo evalúa. Si coincide con la trama esperada, modifica una variable global.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Mensajes ( void )
{
   	int dato = 0;

   	static uint8_t ESTADO = INICIO_TRAMA, valor = -1;

   	dato = PopRx( );

	if( dato >= 0 )
	{
		switch ( ESTADO )
		{
			case INICIO_TRAMA :
				if ( dato == '#' )
					ESTADO = TRAMA;
				break;
			case TRAMA :
				valor = ( dato - '0' );
				rfid_validate = valor;
				ESTADO = FIN_TRAMA;
				break;
			case FIN_TRAMA :
				ESTADO = INICIO_TRAMA;
				if ( dato != '$' )
					valor = -1;
				break;
		}
	}

	if ( valor == 1 )
	{
		DisplayLCD ( "     USUARIO    " , RENGLON_1 , 0 );
		DisplayLCD ( "   AUTORIZADO   " , RENGLON_2 , 0 );
	}

	if ( valor == 0 )
	{
		DisplayLCD ( "    USUARIO    " , RENGLON_1 , 0 );
		DisplayLCD ( "NO IDENTIFICADO" , RENGLON_2 , 0 );
	}

	valor = -1;
}
