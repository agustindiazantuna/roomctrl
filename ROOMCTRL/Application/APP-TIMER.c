 /**
 	\file APP-TIMER.c
 	\brief Aplicación de la máquina de timer.
 	\details Contiene la función TimerEvent(), aplicación de la máquina de timer.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile timers_t Timers[ C_TIMERS ];
	extern volatile uint8_t dht_on;
	extern volatile uint8_t hcsr04_on;
	extern volatile uint8_t hcsr04_expired;
	extern volatile uint8_t rfid_validate;
	extern volatile uint8_t buzzer_detection;
	extern volatile uint8_t led_green;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void TimerEvent ( void )
	\brief Función aplicación de la máquina de timers.
 	\details Contiene un switch encargado de analizar el estado de cada timer y en el caso de la finalización, realizar la acción correspondiente.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void TimerEvent ( void )
{
	uint8_t i;

	for ( i = 0 ; i < C_TIMERS ; i ++ )
	{
		if ( Timers[ i ].Event )
		{
			switch ( i )
			{
				case E_T1:										// Timer de DHT11.
					if( dht_on == 0 )
						dht_on = 1;
					break;
				case E_T2:										// Timer HCSR04: Usuario detectado, tiempo hasta que apoya la tarjeta.
					hcsr04_expired = 1;
					break;
				case E_T3:										// Timer HCSR04: Tarjeta detectada, tiempo hasta que abre la puerta.
					hcsr04_on = 1;
					rfid_validate = 0;
					led_green = 0;
					DisplayLCD( "    RoomCTRL    " , RENGLON_1 , 0 );
					DisplayLCD( "  Ready to use  " , RENGLON_2 , 0 );
					break;
				case E_T4:										// Timer HCSR04: Se desactiva el sensor mientras sale el usuario.
					hcsr04_on = 1;
					DisplayLCD( "    RoomCTRL    " , RENGLON_1 , 0 );
					DisplayLCD( "  Ready to use  " , RENGLON_2 , 0 );
					break;
				case E_T5:
					hcsr04_on = 1;
					break;
			}
			Timers[ i ].Event = 0;
		}
	}
}

