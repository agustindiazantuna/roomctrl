 /**
 	\file APP-RDM6300.c
 	\brief Aplicación de control del lector de tarjetas rfid.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	volatile uint8_t Trama[SIZE_BUFF_SERIE];
	extern volatile uint8_t rdm6300_on;

	extern int FlagTrama;

	extern volatile unsigned char BufferRx0[TOPE_BUFFER_RX0];

	extern volatile uint8_t transmit_id;
	extern volatile uint8_t transmit_new_id;


// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void App_RDM6300 ( uint8_t button )
	\brief Función aplicación del sensor de rfid.
 	\details Máquina de estados de armado de la trama leída de la tarjeta, según los ascii de start y stop.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param button : Tipo uint8_t, contiene el botón pulsado.
	\return void
*/

void App_RDM6300 ( uint8_t button )
{
	static int first = 1, new_id = 0;
	volatile int dato;
	static uint8_t STATE_TRAMA = INICIO;
	volatile static uint8_t posTrama = 0;

	if ( button == 2 )
	{
		rdm6300_on = 1;
		new_id = 1;
	}

	if ( rdm6300_on )
	{
		int i;
		if ( first )
		{
			for ( i = 0 ; i < TOPE_BUFFER_RX0 ; i++ )
				BufferRx0[i] = 0;
			first = 0;
		}

		dato = PopRx0( );

		if( dato >= 0 )
		{
			switch ( STATE_TRAMA )
			{
				case INICIO:
					if ( dato == INICIO_TRAMA0 )
					{
						STATE_TRAMA = CARGA;
						posTrama = 0;
					}
					break;
				case CARGA:
					switch ( dato )
					{
						case INICIO_TRAMA0:
							posTrama = 0;
							break;
						case FIN_TRAMA0:
							STATE_TRAMA = INICIO;
							if ( new_id )
							{
								transmit_new_id = 1;	// Caso de ingreso de nuevo usuario.
								new_id = 0;
							}
							else
								transmit_id = 1;
							rdm6300_on = 0;
							first = 1;
							return;
						default:
							Trama[posTrama] = dato ;	// Guardo la trama.
							posTrama++;
							break;
					}
					break;
			}
		}
	}
}
