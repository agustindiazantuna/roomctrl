 /**
 	\file Infotronic.h
 	\brief Header general del proyecto.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

#ifndef KIT_INFO2_H_
#define KIT_INFO2_H_

#include "Oscilador.h"
#include "RegsLPC1769.h"
#include "UART0.h"
#include "UART1.h"
#include "Timer.h"
#include "LCD.h"
#include "DHT11.h"
#include "HCSR04.h"
#include "RDM6300.h"

// GENERAL MACROS
#define		ON				1
#define		OFF				0
#define		ALTO			1
#define		BAJO			0
#define		NO_KEY			0xFF

// ----------------------------------------------------KIT INFOTRONIC---------------------------------------------------- //
// INIT
void Init_Kit ( void );

// PIN KIT
	// Led RGB:
		#define		RGBR				PORT2,2
		#define		RGBG				PORT2,1
		#define		RGBB				PORT2,3

	// Relays
		#define		RELAY0				PORT2,0
		#define		RELAY1				PORT0,23
		#define		RELAY2				PORT0,21
		#define		RELAY3				PORT0,27

	// Buzzer
		#define		BUZZ				PORT0,28

	// Teclas (teclado 4x1)
		#define		SW1					PORT2,10
		#define		SW2					PORT0,18
		#define		SW3					PORT0,11
		#define		SW4					PORT2,13
		#define		SW5					PORT1,26

// MACROS
#define		CANT_ENTRADAS	8
#define		CANT_REBOTES	4

// FW
uint8_t TecladoHW_KIT ( void );			//TECLADO COMUN
void Debounce_KIT ( void );

// PR
uint8_t Tecla_KIT ( void );


// -----------------------------------------------------EXPANSIONES----------------------------------------------------- //
// INIT
void Init_Expansion ( void );

// PIN EXPANSIONES:
	#define		EXPANSION0			PORT2,7
	#define 	EXPANSION1			PORT1,29
	#define		EXPANSION2			PORT4,28
	#define		EXPANSION3			PORT1,23
	#define		EXPANSION4			PORT1,20
	#define		EXPANSION5			PORT0,19
	#define		EXPANSION6			PORT3,26
	#define		EXPANSION7			PORT1,25
	#define		EXPANSION8			PORT1,22
	#define		EXPANSION9			PORT1,19
	#define		EXPANSION10			PORT0,20
	#define		EXPANSION11			PORT3,25
	#define		EXPANSION12			PORT1,27
	#define		EXPANSION13			PORT1,24
	#define		EXPANSION14			PORT1,21
	#define		EXPANSION15			PORT1,18
	#define		EXPANSION16			PORT1,31
	#define		EXPANSION17			PORT0,24
	#define		EXPANSION18			PORT0,25
	#define		EXPANSION19			PORT0,17
	#define		EXPANSION20			PORT1,31
	#define		EXPANSION21			PORT0,22
	#define		EXPANSION22			PORT0,15
	#define		EXPANSION23			PORT0,16
	#define		EXPANSION24			EXPANSION23								// VER QUE ONDA
	#define		EXPANSION25			PORT2,12

// EXPANSION 3
		#define 	BCD_A       EXPANSION0	// PLACA EXPANSION 3
		#define		BCD_B       EXPANSION1	// PLACA EXPANSION 3
		#define 	BCD_C       EXPANSION2	// PLACA EXPANSION 3
		#define 	BCD_D       EXPANSION3	// PLACA EXPANSION 3
		#define 	seg_DP      EXPANSION4	// PLACA EXPANSION 3
		#define 	CLK         EXPANSION5	// PLACA EXPANSION 3
		#define 	RST         EXPANSION6	// PLACA EXPANSION 3
		#define 	FILA_0      EXPANSION7	// PLACA EXPANSION 3
		#define 	FILA_1      EXPANSION8	// PLACA EXPANSION 3
		#define 	FILA_2      EXPANSION9	// PLACA EXPANSION 3
		#define 	FILA_3      EXPANSION10	// PLACA EXPANSION 3
		#define 	COL_0       EXPANSION11	// PLACA EXPANSION 3
		#define 	COL_1       EXPANSION12	// PLACA EXPANSION 3

// MACROS
#define		CANT_DIGITOS	6
#define		DSP0			0
#define		DSP1			1

// FW
void BarridoDisplay_E3 (void);
void DriverTeclado_E3 ( void );
unsigned char TecladoHW_E3 ( void );
void BounceTeclado_E3 ( void );

// PR-EXP3
void Display_E3 ( uint32_t Valor , unsigned char dsp );
unsigned char Tecla_E3 ( void );

// APP
void Test_EXP3 ( void );

// VARIABLES
//unsigned char Digito_Display_E3[6] = {0xff,0xff,0xff,0xff,0xff,0xff};

// --------------------------------------------------------CONFIG-------------------------------------------------------- //
// INIT
void SetPINSEL ( uint8_t , uint8_t , uint8_t );
void SetPINMODE ( uint8_t , uint8_t , uint8_t );
void SetDIR ( uint8_t , uint8_t , uint8_t );
void SetPIN ( uint8_t , uint8_t , uint8_t );
uint8_t GetPIN ( uint8_t , uint8_t , uint8_t );

// --------------------------------------------------------TIMER-------------------------------------------------------- //
// INIT
void InitTimer0 ( void );					// TIMER 0 CON CAP0.0 - HCSR04
void InitTimer3 ( void );					// TIMER 3 CON CAP3.1 - DHT11

// ---------------------------------------------------------MAIN--------------------------------------------------------- //
// INIT
void Inicializar ( void );
void RoomCTRL ( void );

// APP
void EstadoInicial ( void );

// -------------------------------------------------------SYSTICK------------------------------------------------------- //
// INIT
void InicSysTick ( void );

// -------------------------------------------------------SYSTICK------------------------------------------------------- //
// APP
void DOOR_App ( uint8_t );

// FW
void Relay ( void );


#endif /* KIT_INFO2_H_ */
