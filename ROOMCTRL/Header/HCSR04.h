 /**
 	\file HCSR04.h
 	\brief Header del sensor HCSR04.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

#ifndef HCSR04_H_
#define HCSR04_H_

// PIN
	#define		HCSR04_ECHO			PORT1,26			// CAP0.0					SW5
	#define		HCSR04_TRIG			PORT1,31			// (AD0.5 - EA0)			JP2

// MACROS
	#define		WAITING			0
	#define		MEASURING		1
	#define		VALIDATING		2

// INIT
	void InitHCSR04 ( void );

// FW
	void HCSR04_Fw ( void );

// PR
	uint8_t HCSR04_Prim ( uint32_t * Dis );

// APP
	void HCSR04_App ( void );

#endif /* HCSR04_H_ */

