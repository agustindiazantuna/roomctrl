 /**
 	\file UART0.h
 	\brief Header de la UART0.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

#ifndef UART0_H_
#define UART0_H_

// MACROS
	#define		U0THR		DIR_U0THR[0]
	#define		U0RBR		DIR_U0RBR[0]
	#define		U0LCR		DIR_U0LCR[0]
	#define		U0LSR		DIR_U0LSR[0]
	#define		U0DLL		DIR_U0DLL[0]
	#define		U0DLM		DIR_U0DLM[0]
	#define     U0IER		DIR_U0IER[0]
	#define     U0IIR		DIR_U0IIR[0]

	#define		U0RDR		(U0LSR&0x01)
	#define		U0THRE		((U0LSR&0x20)>>5)

	#define IER_RBR		0x01
	#define IER_THRE	0x02
	#define IER_RLS		0x04

	#define IIR_PEND	0x01
	#define IIR_RLS		0x03
	#define IIR_RDA		0x02
	#define IIR_CTI		0x06
	#define IIR_THRE	0x01

	#define LSR_RDR		0x01
	#define LSR_OE		0x02
	#define LSR_PE		0x04
	#define LSR_FE		0x08
	#define LSR_BI		0x10
	#define LSR_THRE	0x20
	#define LSR_TEMT	0x40
	#define LSR_RXFE	0x80
	//UART0:
	//0x4001000CUL : Registro de control de la UART0:
	#define		DIR_U0LCR	( ( uint32_t  * ) 0x4000C00CUL )
	//0x40010014UL : Registro de recepcion de la UART0:
	#define		DIR_U0LSR		( ( uint32_t  * ) 0x4000C014UL )
	//0x40010000UL : Parte baja del divisor de la UART0:
	#define		DIR_U0DLL	( ( uint32_t  * ) 0x4000C000UL )
	//0x40010004UL : Parte alta del divisor de la UART0:
	#define		DIR_U0DLM	( ( uint32_t  * ) 0x4000C004UL )
	//0x40010000UL : Registro de recepcion de la UART0:
	#define		DIR_U0RBR		( ( uint32_t  * ) 0x4000C000UL )
	//0x40010000UL : Registro de transmision de la UART0:
	#define		DIR_U0THR		( ( uint32_t  * ) 0x4000C000UL )
	//0xUL : Registro habilitacion de interrupciones de la UART1:
	#define		DIR_U0IER	( ( uint32_t  * ) 0x4000C004UL )
	// Interrupt ID Register
	#define		DIR_U0IIR	( ( uint32_t  * ) 0x4000C008UL )


	#define 	TOPE_BUFFER_TX0			100
	#define		TOPE_BUFFER_RX0			100

// INIT
	void Init_UART0 ( int );

// PR
	int PopTx0( void );
	void PushRx0(unsigned char);

// FW
	void PushTx0(unsigned char);
	int PopRx0( void );

	#define TRUE 			1
	#define FALSE			-1

	#define SIZE_BUFF_SERIE		20

	#define INICIO_TRAMA0 			'\002'
	#define FIN_TRAMA0				'\003'

// APP
	void App_RDM6300 ( uint8_t );

#endif /* UART0_H_ */
