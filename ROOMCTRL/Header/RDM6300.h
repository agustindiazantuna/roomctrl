 /**
 	\file RDM6300.h
 	\brief Header del sensor RDM6300.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

#ifndef RDM6300_H_
#define RDM6300_H_

// MACROS
	#define SIZE_BUFF_SERIE		20
	#define INICIO	0
	#define	CARGA	1

#endif /* RDM6300_H_ */
