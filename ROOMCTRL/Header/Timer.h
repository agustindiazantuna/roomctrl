 /**
 	\file Timer.h
 	\brief Header de la máquina de timers.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

#ifndef TIMER_H_
#define TIMER_H_

// MACROS
	#define		C_TIMERS		5

	#define		DECIMAS		40
	#define		SEGUNDOS	10
	#define		MINUTOS		60

	#define		E_T1			0
	#define		E_T2			1
	#define		E_T3			2
	#define		E_T4			3
	#define		E_T5			4

	#define		T_T1			10
	#define		T_T2			30		// SEG
	#define		T_T3			45		// SEG
	#define		T_T4			10		// SEG
	#define		T_T5			5		// DEC

// TIPODATO

	typedef		uint8_t		EVENT;
	typedef		uint8_t		TIME;
	typedef		uint8_t		UNIT;

	typedef	struct
	{
		EVENT	Event;
		TIME	Time;
		UNIT	Unit;
	}timers_t;

// FW
	void Driver_Timers ( void );
	void AnalizarTimers ( UNIT u );

// PR
	void TimerStart ( EVENT , TIME , UNIT );
	void TimerStop ( EVENT  );
	void TimerClose ( void );

// APP
	void TimerEvent ( void );

#endif /* TIMER_H_ */
