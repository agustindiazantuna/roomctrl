 /**
 	\file LCD.h
 	\brief Header del LCD.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

#ifndef LCD_H_
#define LCD_H_

// PIN LCD:
	#define		LCD_D4		PORT0,5
	#define		LCD_D5		PORT0,10
	#define		LCD_D6		PORT2,4
	#define		LCD_D7		PORT2,5
	#define		LCD_RS		PORT2,6
	#define		LCD_E		PORT0,4

// MACROS
	#define		LCD_DATA		0
	#define		LCD_CONTROL		1
	#define 	RENGLON_1		0
	#define 	RENGLON_2		1
	#define 	LCDBUFFER_SIZE		160

// INIT
	void Init_LCD (void);

// FW
	uint8_t PushLCD ( uint8_t dato , uint8_t control );
	int PopLCD ( void );
	void Dato_LCD ( void );

// PR
	void DisplayLCD ( char * msg , uint8_t r , uint8_t pos );

// APP
	void LCD_App ( void );

// VARIABLES
	volatile uint8_t Buffer_LCD[LCDBUFFER_SIZE];
	volatile uint32_t inxInLCD;
	volatile uint32_t inxOutLCD;
	volatile uint32_t cantidadColaLCD;
	volatile uint32_t Demora_LCD;

#endif /* LCD_H_ */
