 /**
 	\file UART1.h
 	\brief Header del sensor UART1.
 	\author Catedra Info II.
 	\date 2014.11.01
*/

#ifndef UART_H_
#define UART_H_

// PCUART0
	#define PCUART0_POWERON (1 << 3)

	#define PCLK_UART0 6
	#define PCLK_UART0_MASK (3 << 6)

	#define IER_RBR		0x01
	#define IER_THRE	0x02
	#define IER_RLS		0x04

	#define IIR_PEND	0x01
	#define IIR_RLS		0x03
	#define IIR_RDA		0x02
	#define IIR_CTI		0x06
	#define IIR_THRE	0x01

	#define LSR_RDR		0x01
	#define LSR_OE		0x02
	#define LSR_PE		0x04
	#define LSR_FE		0x08
	#define LSR_BI		0x10
	#define LSR_THRE	0x20
	#define LSR_TEMT	0x40
	#define LSR_RXFE	0x80

	// ***********************
	// Function to set up UART

	#define			U1RBR				(*( ( registro_t  * ) 0x40010000UL ))// Receiver Buffer Register. (DLAB = 0)	RO
	#define			U1THR				(*( ( registro_t  * ) 0x40010000UL ))// Transmit Holding Register. (DLAB = 0)	WO
	#define			U1DLL				(*( ( registro_t  * ) 0x40010000UL ))// Divisor Latch LSB. (DLAB = 1)			RW		0x01

	#define			U1IER				(*( ( registro_t  * ) 0x40010004UL ))// Interrupt Enable Register. (DLAB = 0)	RW		NA
	#define			U1DLM				(*( ( registro_t  * ) 0x40010004UL ))// Divisor Latch MSB. (DLAB = 1)			RW		NA

	#define			U1IIR				(*( ( registro_t  * ) 0x40010008UL ))// Interrupt ID Register					RO		0x01
	#define			U1FCR				(*( ( registro_t  * ) 0x40010008UL ))// FIFO Control Register					WO		0x00

	#define			U1LCR				(*( ( registro_t  * ) 0x4001000cUL ))// Line Control Register					RW		0x00

	#define			U1LSR				(*( ( registro_t  * ) 0x40010014UL ))// Line Status Register					RO		0x60

	#define			U1SCR				(*( ( registro_t  * ) 0x4001001cUL ))// Scratch Pad Register					RW		0x00
	#define			U1ACR				(*( ( registro_t  * ) 0x40010020UL ))// Auto-baud Control Register				RW		0x00
	#define			U1ICR				(*( ( registro_t  * ) 0x40010024UL ))// IrDA Control Register					RW		0x00
	#define			U1FDR				(*( ( registro_t  * ) 0x40010028UL ))// Fractional Divider Register				RW		0x10
	#define			U1TER				(*( ( registro_t  * ) 0x40010030UL ))// Transmit Enable Register				RW		0x80

	#define			UART1				( ( registro_t  * ) 0x40010000UL )

	#define	TXD		1
	#define	RXD		1

	#define SYSTEMCORECLOCK		100000000UL	// 100 MHZ

	#define BAUDS				9600            /* Velocidad del Puerto Serie    */

// MACROS
	#define 	TOPE_BUFFER_TX			20
	#define		TOPE_BUFFER_RX			20

	#define		INICIO_TRAMA 			1
	#define		TRAMA		 			2
	#define		FIN_TRAMA	 			3

// INIT
	void Init_UART1 ( int );

// UART1.c
	void TX_Send ( void );

// PR-UART1.c
	void PushTx(unsigned char dato);
	int PopRx( void );

// FW-UART1.c
	void PushRx(unsigned char dato);
	int PopTx( void );

// UART1.c
	void Transmitir ( void );
	void Mensajes(void);

#endif /*UART_H_*/
