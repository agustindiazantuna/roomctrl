 /**
 	\file RegsLPC1769.h
 	\brief Header de los registros del micro LPC1769.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

#ifndef REGS_H_
#define REGS_H_

#define		__RW				volatile
typedef 	unsigned int 		uint32_t;
typedef 	unsigned short 		uint16_t;
typedef 	unsigned char 		uint8_t;
typedef 	__RW uint32_t 		registro_t;  //!< defino un tipo 'registro'.

// MACRO PORT

#define		PORT0		0
#define		PORT1		1
#define		PORT2		2
#define		PORT3		3
#define		PORT4		4

#define		FUNCION_GPIO	0
#define		FUNCION_1		1
#define		FUNCION_2		2
#define		FUNCION_3		3

#define		MODO_0		0
#define		MODO_1		1
#define		MODO_2		2
#define		MODO_3		3

#define		ENTRADA		0
#define		SALIDA		1

//!< ----------- Estados de PINSEL:
//#define		PINSEL_GPIO			0
//#define		PINSEL_FUNC1		1
//#define		PINSEL_FUNC2		2
//#define		PINSEL_FUNC3		3

//!< GPIO - PORT0
	/*	*						*
		*************************
		*		FIODIR			*	0x2009C000
		*************************
		*		RESERVED		*	0x2009C004
		*************************
		*		RESERVED		*	0x2009C008
		*************************
		*		RESERVED		*	0x2009C00C
		*************************
		*		FIOMASK			*	0x2009C010
		*************************
		*		FIOPIN			*	0x2009C014
		*************************
		*		FIOSET			*	0x2009C018
		*************************
		*		FIOCLR			*	0x2009C01C
		*************************
		*						*
		*						*
	*/

// REGISTROS ------------------------------------------------------------------------------------------------------------------------
#define		PINSEL		( ( registro_t  * ) 0x4002C000UL )		//!< Direccion de inicio de los registros PINSEL
#define		PINMODE		( ( registro_t  * ) 0x4002C040UL )		//!< Direccion de inicio de los registros de modo de los pines del GPIO
#define		GPIO		( ( registro_t  * ) 0x2009C000UL )		//!< Direccion de inicio de los registros de GPIOs

// RELAY ----------------------------------------------------------------------------------------------------------------------------
#define		FIO0CLEAR		( * ( ( registro_t  * ) 0x2009C01CUL ) )
#define 	RELAY3_OFF 		FIO0CLEAR|=(1 <<27)


// 0xE000E010UL: Registro de control del SysTick:
#define 	SYSTICK		( (registro_t *) 0xE000E010UL )

#define		STCTRL		SYSTICK[ 0 ]
	#define		ENABLE			0
	#define		TICKINT			1
	#define		CLKSOURCE		2
	#define		COUNTFLAG		16
#define		STRELOAD	SYSTICK[ 1 ]
#define		STCURR		SYSTICK[ 2 ]
#define		STCALIB		SYSTICK[ 3 ]

#define TICKINT_ON    STCTRL |= 0x00000002 // pongo en 1  el bit 1 del STCTRL Hablito   interr del Systick
#define TICKINT_OFF   STCTRL &= 0xFFFFFFFD // pongo en 0  el bit 1 del STCTRL Deshablito interr del Systick

//  NVIC -----------------------------------------------------------------------------------------------------------------
#define		NVIC_TIMER0		1
#define		NVIC_EINT3		21
// Nested Vectored Interrupt Controller (NVIC)
// 0xE000E100UL : Direccion de inicio de los registros de habilitación (set) de interrupciones en el NVIC:
#define		ISER		( ( registro_t  * ) 0xE000E100UL )
// 0xE000E180UL : Direccion de inicio de los registros de deshabilitacion (clear) de interrupciones en el NVIC:
#define		ICER		( ( registro_t  * ) 0xE000E180UL )

// Registros ISER:
#define		ISER0		ISER[0]
	#define		ISER0_UART1			0x00000040
#define		ISER1		ISER[1]

// Registros ICER:
#define		ICER0		ICER[0]
#define		ICER1		ICER[1]

//  Power Control for Peripherals register (PCONP - 0x400F C0C4) [pag. 62 user manual LPC1769]
// 0x400FC0C4UL : Direccion de inicio del registro de habilitación de dispositivos:
#define 	PCONP	(* ( ( registro_t  * ) 0x400FC0C4UL ))
	#define		PCSPI		8

// Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 -0x400F C1A8 and PCLKSEL1 - 0x400F C1AC) [pag. 56 user manual]
// 0x400FC1A8UL : Direccion de inicio de los registros de seleccion de los CLKs de los dispositivos:
#define		PCLKSEL		( ( registro_t  * ) 0x400FC1A8UL )
//!< Registros PCLKSEL
#define		PCLKSEL0	PCLKSEL[0]
	#define		PCLK_SPI	16
#define		PCLKSEL1	PCLKSEL[1]

//  Interrupciones Externas  ------------------------------------------------------------------------------------------------
#define		EXTINT 		( * ( ( registro_t  * ) 0x400FC140UL ) )
	#define		EINT0		0
	#define		EINT1		1
	#define		EINT2		2
	#define		EINT3		3
#define		EXTMODE 	( * ( ( registro_t  * ) 0x400FC148UL ) )
	#define		EXTMODE0	0
	#define		EXTMODE1	1
	#define		EXTMODE2	2
	#define		EXTMODE3	3
#define		EXTPOLAR 	( * ( ( registro_t  * ) 0x400FC14CUL ) )
	#define		EXTPOLAR0	0
	#define		EXTPOLAR1	1
	#define		EXTPOLAR2	2
	#define		EXTPOLAR3	3

///////////////////////////   SIN INTERRUPCIONES    ///////////////////////////////

//0x4008C000UL : Registro de conversion del DAC:
#define		DIR_DACR	( ( uint32_t  * ) 0x4008C000UL )
//0x4008C004UL : Registro de control del DAC:
#define		DIR_DACCTRL	( ( uint32_t  * ) 0x4008C004UL )
//0x4008C008UL : Registro de contador del DAC:
#define		DIR_DACCNTVAL ( ( uint32_t  * ) 0x4008C008UL )

//0x40034000UL: Registro de control del ADC:
#define		DIR_AD0CR	( ( uint32_t  * ) 0x40034000UL )
//0x40034004UL: Registro de estado del ADC:
#define		DIR_AD0GDR	( ( uint32_t  * ) 0x40034004UL )
//0x4003400CUL: Registro de interrupcion del ADC
#define		DIR_AD0INTEN ( ( uint32_t  * ) 0x4003400CUL )
//0x40034010UL: Registros de estado de los ADCx
#define		AD0DR		( ( uint32_t  * ) 0x40034010UL )

//Registros del DAC:
#define		DACR		DIR_DACR[0]
#define		DACCTRL		DIR_DACCTRL[0]
#define		DACCNTVAL	DIR_DACCNTVAL[0]

//Registros del ADC:
#define		AD0CR		DIR_AD0CR[0]
#define		AD0GDR		DIR_AD0GDR[0]
#define		AD0INTEN	DIR_AD0INTEN[0]
#define		AD0STAT		AD0DR[8]

#define		AD0DR0		AD0DR[0]
#define		AD0DR1		AD0DR[1]
#define		AD0DR2		AD0DR[2]
#define		AD0DR3		AD0DR[3]
#define		AD0DR4		AD0DR[4]
#define		AD0DR5		AD0DR[5]
#define		AD0DR6		AD0DR[6]
#define		AD0DR7		AD0DR[7]

//!< ///////////////////   TIMERS   /////////////////////////////
//!< 0x40004000UL : Direccion de inicio de los registros del Timer0
#define		TIMER0		( ( registro_t  * ) 0x40004000UL )
//!< 0x40008000UL : Direccion de inicio de los registros del Timer1
#define		TIMER1		( ( registro_t  * ) 0x40008000UL )
//!< 0x40090000UL : Direccion de inicio de los registros del Timer2
#define		TIMER2		( ( registro_t  * ) 0x40090000UL )
//!< 0x40094000UL : Direccion de inicio de los registros del Timer3
#define		TIMER3		( ( registro_t  * ) 0x40094000UL )

// TIMER0
#define		T0IR		TIMER0[0]
	#define		IRMR0		0
	#define		IRMR1		1
	#define		IRMR2		2
	#define		IRMR3		3
	#define		IRCR0		4
	#define		IRCR1		5
#define		T0TCR		TIMER0[1]
	#define		CE			0
	#define		CR			1
#define		T0TC		TIMER0[2]
#define		T0PR		TIMER0[3]
#define		T0PC		TIMER0[4]
#define		T0MCR		TIMER0[5]
	#define		MR0I		0
	#define		MR0R		1
	#define		MR0S		2
	#define		MR1I		3
	#define		MR1R		4
	#define		MR1S		5
	#define		MR2I		6
	#define		MR2R		7
	#define		MR2S		8
	#define		MR3I		9
	#define		MR3R		10
	#define		MR3S		11
#define		T0MR0		TIMER0[6]
#define		T0MR1		TIMER0[7]
#define		T0MR2		TIMER0[8]
#define		T0MR3		TIMER0[9]
#define		T0CCR		TIMER0[10]
#define		T0CR0		TIMER0[11]
#define		T0CR1		TIMER0[12]
#define		T0EMR		( * ( ( registro_t  * ) 0x4000403CUL ) )
#define		T0CTCR		( * ( ( registro_t  * ) 0x40004070UL ) )/** CTCR - COUNT CONTROL REGISTER */
	#define		TCM			0
	#define		CIS			2

// TIMER1
#define		T1IR		TIMER1[0]
#define		T1TCR		TIMER1[1]
#define		T1TC		TIMER1[2]
#define		T1PR		TIMER1[3]
#define		T1PC		TIMER1[4]
#define		T1MCR		TIMER1[5]
#define		T1MR0		TIMER1[6]
#define		T1MR1		TIMER1[7]
#define		T1MR2		TIMER1[8]
#define		T1MR3		TIMER1[9]
#define		T1CCR		TIMER1[10]
#define		T1CR0		TIMER1[11]
#define		T1CR1		TIMER1[12]
//#define		T0EMR		( * ( ( registro_t  * ) 0x4000403CUL ) )
#define		T1CTCR		( * ( ( registro_t  * ) 0x40008070UL ) )/** CTCR - COUNT CONTROL REGISTER */

// TIMER2
#define		T2IR		TIMER2[0]
#define		T2TCR		TIMER2[1]
#define		T2TC		TIMER2[2]
#define		T2PR		TIMER2[3]
#define		T2PC		TIMER2[4]
#define		T2MCR		TIMER2[5]
#define		T2MR0		TIMER2[6]
#define		T2MR1		TIMER2[7]
#define		T2MR2		TIMER2[8]
#define		T2MR3		TIMER2[9]
#define		T2CCR		TIMER2[10]
#define		T2CR0		TIMER2[11]
#define		T2CR1		TIMER2[12]
//#define		T0EMR		( * ( ( registro_t  * ) 0x4000403CUL ) )
#define		T2CTCR		( * ( ( registro_t  * ) 0x40090070UL ) )/** CTCR - COUNT CONTROL REGISTER */

// TIMER03
#define		T3IR		TIMER3[0]
#define		T3TCR		TIMER3[1]
#define		T3TC		TIMER3[2]
#define		T3PR		TIMER3[3]
#define		T3PC		TIMER3[4]
#define		T3MCR		TIMER3[5]
#define		T3MR0		TIMER3[6]
#define		T3MR1		TIMER3[7]
#define		T3MR2		TIMER3[8]
#define		T3MR3		TIMER3[9]
#define		T3CCR		TIMER3[10]
#define		T3CR0		TIMER3[11]
#define		T3CR1		TIMER3[12]
//#define		T0EMR		( * ( ( registro_t  * ) 0x4000403CUL ) )
//!< EMR no han sido definidos. (no están en posiciones contiguas)
#define		T3CTCR		( * ( ( registro_t  * ) 0x40094070UL ) )/** CTCR - COUNT CONTROL REGISTER */

#endif
