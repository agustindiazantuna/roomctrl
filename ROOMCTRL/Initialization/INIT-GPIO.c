 /**
 	\file INIT-GPIO.c
 	\brief Archivo con la inicialización de las funciones de manejo de hardware.
 	\details Contiene funciones de acceso a hardware.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void void SetDIR( uint8_t puerto , uint8_t pin , uint8_t dir )
	\brief Establece si un determinado PIN de un determinado PUERTO (previamente configurado como GPIO) es entrada o salida.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param puerto: Tipo uint8_t, puerto con el que se va a trabajar
 	\param pin: Tipo uint8_t, pin a configurar sentido
 	\param dir: Tipo uint8_t, Direccion 0 = entrada - 1 = salida.
 	\return void
*/

void SetDIR( uint8_t puerto , uint8_t pin , uint8_t dir )
{
	puerto = puerto * 8;

	GPIO[ puerto ] = GPIO[ puerto ] & ( ~ ( 1 << pin ) );
	GPIO[ puerto ] = GPIO[ puerto ] | ( dir << pin );
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void SetPIN ( uint8_t puerto , uint8_t pin , uint8_t estado )
	\brief Establece un ESTADO en un determinado PIN de un determinado PUERTO.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param puerto: Tipo uint8_t, puerto con el que se va a trabajar
 	\param pin: Tipo uint8_t, pin a configurar sentido
 	\param estado: Tipo uint8_t, valor a establecer en el PIN del PUERTO [0-1].
 	\return void
*/

void SetPIN( uint8_t puerto , uint8_t pin , uint8_t estado )
{
	puerto = puerto * 8 + 5;

	GPIO[ puerto ] = GPIO[ puerto ] & ( ~ ( 1 << pin ) );
	GPIO[ puerto ] = GPIO[ puerto ] | ( estado << pin );
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  uint8_t GetPIN( uint8_t puerto , uint8_t pin , uint8_t actividad )
	\brief Devuelve el ESTADO de un determinado PIN de un determinado PUERTO.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param puerto: Tipo uint8_t, puerto con el que se va a trabajar
 	\param pin: Tipo uint8_t, pin a configurar sentido
 	\param actividad: Tipo uint8_t, ALTO = 1, BAJO = 0.
 	\return Estado del pin consultado (uint_8).
*/

uint8_t GetPIN( uint8_t puerto , uint8_t pin , uint8_t actividad )
{
	puerto = puerto * 8 + 5;

	return ( ( ( GPIO[ puerto ] >> pin ) & 1 ) == actividad ) ? 1 : 0;
}
