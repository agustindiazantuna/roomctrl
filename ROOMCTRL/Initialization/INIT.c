 /**
 	\file INIT.c
 	\brief Archivo con función de inicialización.
 	\details Contiene la función Inicializar().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Inicializar ( void )
	\brief Función inicialización del kit.
 	\details Función que contiene las funciones de inicialización de cada periférico y establece el estado inicial.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Inicializar ( void )
{
	InitPLL ( );					// Oscilador
	InicSysTick ( );
	Init_UART0 ( BAUDS );			// 9600 BAUDIOS
	Init_UART1 ( BAUDS );			// 9600 BAUDIOS
	Init_Kit ( ) ;
	Init_Expansion ( );
	Init_LCD ( );
	InitHCSR04 ( );
	InitDHT11 ( );					// Tiene demora dentro
	InitTimer0 ( );					// HCSR04
	InitTimer3 ( );					// DHT11

	EstadoInicial ( );
}
