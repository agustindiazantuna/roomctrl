 /**
 	\file INIT-UART0.c
 	\brief Archivo con función de inicialización de la UART0.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Init_UART1 ( int baudrate )
	\brief Función de configuración de la UART1 para el sensor RDM6300.
	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
	\date 2014.11.01
	\param baudrate : Tipo int, baudrate de conexión.
	\return void
*/

void Init_UART1 (int baudrate)
{
	int pclk,Fdiv;

	// PCLK_UART0 is being set to 1/4 of SystemCoreClock
	pclk = SYSTEMCORECLOCK / 4;

	// Enciendo la UART0 (On por default)
	///LPC_SC->PCONP |=  PCUART0_POWERON;

	//Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 - 0x400FC1A8 and PCLKSEL1 - 0x400FC1AC)
	// A pair of bits in a Peripheral Clock Selection register controls the rate of the clock signal
	// that will be supplied to the corresponding peripheral as specified in Table 40, Table 41 and
	// 00 PCLK_peripheral = CCLK/4 (Reset value)
	// 01 PCLK_peripheral = CCLK
	// 10 PCLK_peripheral = CCLK/2
	// 11 PCLK_peripheral = CCLK/8, except for CAN1, CAN2, and CAN filtering when “11” selects = CCLK/6.

	// Turn on UART0 peripheral clock
	///LPC_SC->PCLKSEL0 &= ~(PCLK_UART0_MASK);
	///LPC_SC->PCLKSEL0 |=  (0 << PCLK_UART0);		// PCLK_periph = CCLK/4

	// Seleccion de los pines como Tx y Rx de la UART1
	SetPINSEL ( 0 , 15 , TXD );
	SetPINSEL ( 0 , 16 , RXD );

	U1LCR = 0x83;						// 8 bits, no Parity, 1 Stop bit, DLAB=1 - Pone un 1 en el primer bit con el 8, despues lo saca

	// Sin divisor Fraccional
	Fdiv = ( pclk / 16 ) / baudrate ;	// FR = 1
    U1DLM = Fdiv / 256;					// Primeros 8 bits del baudrate
    U1DLL = Fdiv % 256;					// Segundos 8 bits del baudrate - BR medida = 9642 con un Er = 0.44% - MUY BUENO !!!

    U1LCR = 0x03;						// 8 bits, no Parity, 1 Stop bit DLAB = 0

    U1FCR = 0x00; 						// FIFO disabled

    U1IER = IER_RBR | IER_THRE ;		// Habilito interrupciones. Es lo mismo U1IER = 0x03; Habilita la int de RX y TX.
    ISER0 |= ISER0_UART1;				// Habilito interrupciones Vector de iterrupciones
}
