/*
 * INIT-SYSTICK.c
 *
 *  Created on: 28/08/2014
 *      Author: Agustin Diaz Antuña
 */

 /**
 	\file INIT-SYSTICK.c
 	\brief Driver que configura el systick.
 	\details Contiene la función InicSysTick().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void InicSysTick ( void )
	\brief Función que configura el Systick.
 	\details Se configura a 2,5 milisegundos.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param void
	\return void
*/

void InicSysTick ( void )
{
	STRELOAD = ( ( STCALIB / 4) ) - 1 ;	// Recarga cada 2.5 ms
	STCURR = 0;	// Cargando con cero limpio y provoco el disparo de una intrrupcion
	// Habilito el conteo
	// Habilito interrupcion
	// Utilizamos Pclk
	STCTRL |=  ( ( 1 << ENABLE ) | ( 1 << TICKINT ) | ( 1 << CLKSOURCE ) );
}
