 /**
 	\file INIT-STATE.c
 	\brief Driver que configura un estado inicial del hardware.
 	\details Contiene la función EstadoInicial().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t dht_on;
	extern volatile uint8_t hcsr04_on;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void EstadoInicial ( void )
	\brief Función driver que configura el estado inicial del kit Infotronic V1.02.
 	\details Establece un estado inicial del display 7 segmentos, del LCD, prueba el funcionamiento del relay, buzzer y led RGB.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void EstadoInicial ( void )
{
	char msg_1[16] = "RoomCTRL";
	char msg_2[16] = "Ready to use";

	DisplayLCD( msg_1 , RENGLON_1 , 4 );
	DisplayLCD( msg_2 , RENGLON_2 , 2 );

	Display_E3 ( 000 , DSP0 );
	Display_E3 ( 000 , DSP1 );

	SetPIN ( BUZZ , ON );						// SE LO APAGA

	SetPIN ( RGBR , ON );
	SetPIN ( RGBB , OFF );
	SetPIN ( RGBG , OFF );

	SetPIN ( RELAY0 , OFF );
	SetPIN ( RELAY1 , OFF );
	SetPIN ( RELAY2 , OFF );
	RELAY3_OFF;

	SetPIN ( RELAY0 , ON );

	hcsr04_on = 1;
	dht_on = 1;
}
