 /**
 	\file INIT-CONFIG.c
 	\brief Archivo con la inicialización de las funciones de configuración.
 	\details Contiene funciones de acceso a hardware.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void SetPINSEL ( uint8_t puerto , uint8_t pin, uint8_t sel )
	\brief Selección de modo de los puertos (4 modos).
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param puerto: Tipo uint8_t, port a configurar.
 	\param pin:	Tipo uint8_t, pin del port a configurar.
 	\param funcion:	Tipo uint8_t, selección de la funcion que tendra el pin  [0 - 3].
 	\return void
*/

void SetPINSEL ( uint8_t puerto , uint8_t pin ,uint8_t funcion )
{
	puerto = puerto * 2 + pin / 16;
	pin = ( pin % 16 ) * 2;
	PINSEL[ puerto ] = PINSEL[ puerto ] & ( ~ ( 3 << pin ) );
	PINSEL[ puerto ] = PINSEL[ puerto ] | ( funcion << pin );
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void SetPINMODE ( uint8_t port, uint8_t pin, uint8_t modo )
	\brief Selección de modo de los puertos cuando fueron configurados como entradas
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param puerto: Tipo uint8_t, port a configurar.
 	\param pin:	Tipo uint8_t, pin del port a configurar.
 	\param funcion:	Tipo uint8_t, selección de la funcion que tendra el pin  [0 - 3].
 	\return void
*/

void SetPINMODE ( uint8_t port , uint8_t pin ,uint8_t modo)
{
	port = port * 2 + pin / 16;
	pin = ( pin % 16 ) * 2;
	PINMODE[ port ] = PINMODE[ port ] & ( ~ ( 3 << pin ) );
	PINMODE[ port ] = PINMODE[ port ] | ( modo << pin );
}
