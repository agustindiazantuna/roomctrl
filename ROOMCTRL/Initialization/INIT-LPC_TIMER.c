 /**
 	\file INIT-LPC_TIMER.c
 	\brief Archivo con función de inicialización de los timers.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void InitTimer0 ( void )
	\brief Función de configuración del timer 0, utilizado para el DHT11.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void InitTimer0 ( void )				// TIMER 0 CON CAP0.0
{
	PCONP |= 1 << 1; 					// Habilitar Timer 0

	PCLKSEL0 = 0; 						// Clock de timer PCLK = CCLK/4 = 25 mhz

	//-------------COUNT CONTROL REGISTER----------
	T0CTCR = 0x00;						// Timer cuenta pulsos PCLK
	//-------------CAPTURE CONTROL REGISTER----------
	T0CCR = 0x07;						// Habilita int de Captura para CAP0.0 por falling edge y rising edge
	//-------------TIME CONTROL REGISTER----------
	T0TCR = 0x03;						// Apago y reseteo el temporizador

	ISER0 |= (0x01 << 1);   			// Habilito Interrupcion TIMER0
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void InitTimer3 ( void )
	\brief Función de configuración del timer 3, utilizado para el HCSR04.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void InitTimer3 ( void )				// TIMER 3 CON CAP3.1
{
	PCONP |= 1 << 23; 					// Habilitar Timer 3

	PCLKSEL1 = 0; 						// Clock de timer PCLK = CCLK/4 = 25 mhz

	//-------------COUNT CONTROL REGISTER----------
	T3CTCR = 0x00;						// Timer cuenta pulsos PCLK
	//-------------CAPTURE CONTROL REGISTER----------
//	T3CCR = 0x38;						// Habilita int de Captura para CAP3.1 por falling edge y rising edge
	//-------------TIME CONTROL REGISTER----------
	T3TCR = 0x03;						// Apago y reseteo el temporizador

	ISER0 |= (0x01 << 4);   			// Habilito Interrupcion TIMER3
}
