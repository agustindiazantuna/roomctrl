 /**
 	\file INIT-KIT.c
 	\brief Archivo con función de inicialización del Kit Infotronic.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Init_Kit ( void )
	\brief Función de configuración de los pines del Kit.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Init_Kit ( void )
{
	// TECLADO
	SetPINSEL ( SW1 , FUNCION_GPIO );
	SetDIR ( SW1 , ENTRADA );

	SetPINSEL ( SW2 , FUNCION_GPIO );
	SetDIR ( SW2 , ENTRADA );

	SetPINSEL ( SW3 , FUNCION_GPIO );
	SetDIR ( SW3 , ENTRADA );

	SetPINSEL ( SW4 , FUNCION_GPIO );
	SetDIR ( SW4 , ENTRADA );

//	SetPINSEL ( SW5 , FUNCION_GPIO );
//	SetDIR ( SW5 , ENTRADA );

	// LEDs RGB:
	SetPINSEL ( RGBR , FUNCION_GPIO );
	SetDIR ( RGBR , SALIDA );
	SetPIN ( RGBR , ON );

	SetPINSEL ( RGBG , FUNCION_GPIO );
	SetDIR ( RGBG , SALIDA );
	SetPIN ( RGBG , ON );

	SetPINSEL ( RGBB , FUNCION_GPIO );
	SetDIR ( RGBB , SALIDA );
	SetPIN ( RGBB , ON );

	// BUZZER
	SetPINSEL ( BUZZ , FUNCION_GPIO );
	SetDIR ( BUZZ , SALIDA );
	SetPIN ( BUZZ , OFF );

	// RELAYS
	SetPINSEL ( RELAY0 , FUNCION_GPIO );
	SetPINSEL ( RELAY1 , FUNCION_GPIO );
	SetPINSEL ( RELAY2 , FUNCION_GPIO );
	SetPINSEL ( RELAY3 , FUNCION_GPIO );

	SetDIR ( RELAY0 , SALIDA );
	SetDIR ( RELAY1 , SALIDA );
	SetDIR ( RELAY2 , SALIDA );
	SetDIR ( RELAY3 , SALIDA );

	SetPIN ( RELAY0 , ON );		// RELAY 1
	SetPIN ( RELAY1 , ON );		// RELAY 2
	SetPIN ( RELAY2 , ON );		// RELAY 0
	SetPIN ( RELAY3 , ON );		// RELAY 3
}
