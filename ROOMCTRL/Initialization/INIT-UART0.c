 /**
 	\file INIT-UART0.c
 	\brief Archivo con función de inicialización de la UART0.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile unsigned char BufferTx0[TOPE_BUFFER_TX0];		//!< Buffer de Transmision
	extern volatile unsigned char BufferRx0[TOPE_BUFFER_RX0];		//!< Buffer de Recepcion
	extern volatile unsigned char IndiceTxIn0,IndiceTxOut0;		//!< Indices de Transmision
	extern volatile unsigned char IndiceRxIn0,IndiceRxOut0;		//!< Indices de Recepcion
	extern volatile char TxStart0;								//!< flag de fin de TX
	extern volatile uint8_t	BufferSalidas0;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Init_UART0 ( int baudrate )
	\brief Función de configuración de la UART0 para el sensor RDM6300.
	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
	\date 2014.11.01
	\param baudrate : Tipo int, baudrate de conexión.
	\return void
*/

void Init_UART0 (int baudrate)
{
	int pclk,Fdiv , i;

	// PCLK_UART0 is being set to 1/4 of SystemCoreClock
	pclk = SYSTEMCORECLOCK / 4;

	// Enciendo la UART0 (On por default)
	///LPC_SC->PCONP |=  PCUART0_POWERON;

	//1.- Registro PCONP: Energizo la UART:
	PCONP |= 0x01<<3;

	//2.- Registro PCLKSEL0 (0x400FC1A8) - selecciono el clk de la UART (recordar que cclk = 100MHz)
	PCLKSEL0 &= ~(0x03<<8);   /// dejo el clock/4 po eso no daba y funcionaba con el hyperterminal a 2400
//	PCLKSEL0 |= (0x00<<8);    //tabla 42 dejo el clock a 4000000


	//Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 - 0x400FC1A8 and PCLKSEL1 - 0x400FC1AC)
	// A pair of bits in a Peripheral Clock Selection register controls the rate of the clock signal
	// that will be supplied to the corresponding peripheral as specified in Table 40, Table 41 and
	// 00 PCLK_peripheral = CCLK/4 (Reset value)
	// 01 PCLK_peripheral = CCLK
	// 10 PCLK_peripheral = CCLK/2
	// 11 PCLK_peripheral = CCLK/8, except for CAN1, CAN2, and CAN filtering when “11” selects = CCLK/6.

	// Turn on UART0 peripheral clock
	///LPC_SC->PCLKSEL0 &= ~(PCLK_UART0_MASK);
	///LPC_SC->PCLKSEL0 |=  (0 << PCLK_UART0);		// PCLK_periph = CCLK/4

	// Seleccion de los pines como Tx y Rx de la UART1
	SetPINSEL ( 0 , 2 , TXD );
	SetPINSEL ( 0 , 3 , RXD );

	U0LCR= 1<<7;						//  DLAB=1

	// Sin divisor Fraccional
	Fdiv = ( pclk / 16 ) / baudrate ;	// FR = 1
    U0DLM = Fdiv / 256;					// Primeros 8 bits del baudrate
    U0DLL = Fdiv % 256;					// Segundos 8 bits del baudrate - BR medida = 9642 con un Er = 0.44% - MUY BUENO !!!

    U0LCR = 0x03;						// 8 bits, no Parity, 1 Stop bit DLAB = 0

    U0IER = 0x03;						// Habilito interrupciones. Es lo mismo U1IER = 0x03; Habilita la int de RX y TX.

    ISER0 |= 1<<5;						// Habilito interrupciones Vector de iterrupciones

	for(i=0; i < TOPE_BUFFER_TX0; i++)
		BufferTx0[i]=0;
}
