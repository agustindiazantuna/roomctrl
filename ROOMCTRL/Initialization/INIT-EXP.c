 /**
 	\file INIT-EXP.c
 	\brief Archivo con la inicialización de la expansión 3.
 	\details Contiene la función de inicialización.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Init_Expansion ( void )
	\brief Función inicialización de la expansión 3.
 	\details Configura los pines a usar en la expansión.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Init_Expansion ( void )
{
	SetPINSEL( BCD_A , FUNCION_GPIO );
	SetPINSEL( BCD_B , FUNCION_GPIO );
	SetPINSEL( BCD_C , FUNCION_GPIO );
	SetPINSEL( BCD_D , FUNCION_GPIO );
	SetPINSEL( seg_DP , FUNCION_GPIO );
	SetPINSEL( CLK , FUNCION_GPIO );
	SetPINSEL( RST , FUNCION_GPIO );
	SetPINSEL( FILA_0 , FUNCION_GPIO );
	SetPINSEL( FILA_1 , FUNCION_GPIO );
	SetPINSEL( FILA_2 , FUNCION_GPIO );
	SetPINSEL( FILA_3 , FUNCION_GPIO );
	SetPINSEL( COL_0 , FUNCION_GPIO );
	SetPINSEL( COL_1 , FUNCION_GPIO );

	SetDIR( BCD_A , SALIDA );
	SetDIR( BCD_B , SALIDA );
	SetDIR( BCD_C , SALIDA );
	SetDIR( BCD_D , SALIDA );
	SetDIR( seg_DP , SALIDA );
	SetDIR( CLK , SALIDA );
	SetDIR( RST , SALIDA );
	SetDIR( FILA_0 , SALIDA );
	SetDIR( FILA_1 , SALIDA );
	SetDIR( FILA_2 , SALIDA );
	SetDIR( FILA_3 , SALIDA );
	SetDIR( COL_0 , ENTRADA );
	SetDIR( COL_1 , ENTRADA );

	SetPINMODE( COL_0 , MODO_3 );
	SetPINMODE( COL_1 , MODO_3 );
}
