/**
	\file PR-UART1.c
	\brief Primitiva de la UART1.
	\details Contiene la función PushTx(), PopRx().
	\author Catedra Info II.
	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile unsigned char BufferTx[TOPE_BUFFER_TX];		//!< Buffer de Transmision
	extern volatile unsigned char BufferRx[TOPE_BUFFER_RX];		//!< Buffer de Recepcion
	extern volatile unsigned char IndiceTxIn,IndiceTxOut;		//!< Indices de Transmision
	extern volatile unsigned char IndiceRxIn,IndiceRxOut;		//!< Indices de Recepcion
	extern volatile char TxStart;								//!< flag de fin de TX
	extern volatile uint8_t	BufferSalidas;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void PushTx ( unsigned char dato )
	\brief Primitiva de la UART1, si no se está transmitiendo nada, escribe el registro, sino llena el buffer de salida.
	\author Catedra Info II.
	\date 2014.11.01
	\param dato : Tipo undigned char, dato a transmitir.
	\return void
*/

void PushTx(unsigned char dato)
{
	if ( TxStart == 0 )					// Si no se está transmitiendo
	{
		TxStart = 1;					// Activo FLAG
		U1THR = dato;					// Pongo dato en el registro
		return;
	}
	BufferTx[IndiceTxIn] = dato;

	IndiceTxIn ++;
	IndiceTxIn %= TOPE_BUFFER_TX;
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn int PopRx ( void )
	\brief Primitiva de la UART1, si los índices de entrada y salida son distintos, saca un dato del buffer y lo devuelve.
	\author Catedra Info II.
	\date 2014.11.01
	\param void
	\return int : Dato extraido del buffer circular de entrada.
*/

int PopRx( void )
{
	int dato = -1;

	if ( IndiceRxIn != IndiceRxOut )
	{
		dato = (unsigned int) BufferRx[IndiceRxOut];
		IndiceRxOut ++;
		IndiceRxOut %= TOPE_BUFFER_RX;
	}
	return dato;
}
