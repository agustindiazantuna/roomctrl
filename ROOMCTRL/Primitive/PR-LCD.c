/**
	\file PR-LCD.c
	\brief Primitiva del LCD.
	\details Contiene la función DisplayLCD().
	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t Buffer_LCD[LCDBUFFER_SIZE];
	extern volatile uint32_t inxInLCD;
	extern volatile uint32_t inxOutLCD;
	extern volatile uint32_t cantidadColaLCD;
	extern volatile uint32_t Demora_LCD;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void DisplayLCD ( char * msg, uint8_t r , uint8_t pos )
	\brief Primitiva del LCD.
	\author Catedra Info II.
	\date 2014.11.01
	\param msg : Tipo char *, mensaje a mostrar.
	\param r : Tipo uint8_t *, renglón.
	\param pos : Tipo uint8_t *, posición.
	\return void
*/

void DisplayLCD ( char * msg, uint8_t r , uint8_t pos )
{
	uint8_t i ;

	switch ( r )
	{
		case RENGLON_1:
			PushLCD( pos + 0x80 , LCD_CONTROL );
			break;
		case RENGLON_2:
			PushLCD( pos + 0xc0 , LCD_CONTROL );
			break;
	}
	for( i = 0 ; msg[ i ] != '\0' ; i++ )
		PushLCD( msg [ i ] , LCD_DATA );
}
