/**
	\file PR-UART0.c
	\brief Primitiva de la UART0.
	\details Contiene la función PushTx0(), PopRx0().
	\author Catedra Info II.
	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile unsigned char BufferTx0[TOPE_BUFFER_TX0];	//!< Buffer de Transmision
	extern volatile unsigned char BufferRx0[TOPE_BUFFER_RX0];	//!< Buffer de Recepcion
	extern volatile unsigned char IndiceTxIn0,IndiceTxOut0;		//!< Indices de Transmision
	extern volatile unsigned char IndiceRxIn0,IndiceRxOut0;		//!< Indices de Recepcion
	extern volatile char TxStart0;								//!< flag de fin de TX
	extern volatile uint8_t	BufferSalidas0;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void PushTx0 ( unsigned char dato )
	\brief Primitiva de la UART0, si no se está transmitiendo nada, escribe el registro, sino llena el buffer de salida.
	\author Catedra Info II.
	\date 2014.11.01
	\param dato : Tipo undigned char, dato a transmitir.
	\return void
*/

void PushTx0(unsigned char dato)
{
	int temporal;

	BufferTx0[IndiceTxIn0] = dato;

	IndiceTxIn0 ++;
	IndiceTxIn0 %= TOPE_BUFFER_TX0;

	if ( TxStart0 == 0 )
	{
		TxStart0 = 1;

		temporal = PopTx0();		// Tomo el dato a Transmitir

		if ( U0THRE == 1 )
			U0THR =  temporal;		// Si hay dato en la cola lo Transmito
		else
			TxStart0 = 0;
	}
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn int PopRx0 ( void )
	\brief Primitiva de la UART0, si los índices de entrada y salida son distintos, saca un dato del buffer y lo devuelve.
	\author Catedra Info II.
	\date 2014.11.01
	\param void
	\return int : Dato extraido del buffer circular de entrada.
*/

int PopRx0( void )
{
	int dato = -1;

	if ( IndiceRxIn0 != IndiceRxOut0 )
	{
		dato = (unsigned int) BufferRx0[IndiceRxOut0];
		IndiceRxOut0 ++;
		IndiceRxOut0 %= TOPE_BUFFER_RX0;
	}
	return dato;
}
