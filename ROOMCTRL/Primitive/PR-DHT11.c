 /**
 	\file PR-DHT11.c
 	\brief Primitiva del sensor de humedad y temperatura DHT11.
 	\details Contiene la función DHT_Prim().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn uint8_t DHT_Prim ( uint8_t * Hum , uint8_t * Tem )
	\brief Primitiva del sensor DHT11.
 	\details Activa los flags de transmición de humedad y temperatura.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param Hum : Tipo uint8_t *, puntero donde guardo el contenido del buffer.
 	\param Tem : Tipo uint8_t *, puntero donde guardo el contenido del buffer.
	\return uint8_t : 1 si la lectura fue nueva y 0 si no lo fue.
*/

uint8_t DHT_Prim ( uint8_t * Hum , uint8_t * Tem )
{
	*Hum = BufferDHT_Hum;
	*Tem = BufferDHT_Tem;

	if ( dht_ok )
	{
		dht_ok = 0;
		transmit_humidity = 1;
		transmit_temperature = 1;
		return 1;
	}
	return 0;
}
