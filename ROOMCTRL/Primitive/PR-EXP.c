 /**
 	\file PR-EXP.c
 	\brief Primitiva de la expansión 3.
 	\details Contiene la primitiva del display y del teclado.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern uint8_t Digito_Display_E3 [CANT_DIGITOS];

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Display_E3 ( uint32_t Valor , unsigned char dsp )
	\brief Primitiva del display de la expansión 3.
	\details Maneja el display como si estuviera partido en dos displays distintos.
	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
	\date 2014.11.01
	\param Valor : Tipo uint8_t *, valor a mostrar.
	\param dsp : Tipo uint8_t *, display donde se mostrará.
	\return void
*/

void Display_E3 ( uint32_t Valor , unsigned char dsp )
{
	unsigned char i;
	char aux[4];
	aux[0] = aux[1] = aux[2] = aux[3] = 0;

	for(i=2; i ;i--)				//Separamos en unidad, decena, centena
	{
		aux[i]=Valor%10;
		Valor/=10;
	}
	aux[0]=Valor;

	switch(dsp)
	{
		case 0 :					//DSP1
			Digito_Display_E3[2] = aux[2];
			Digito_Display_E3[1] = aux[1];
			Digito_Display_E3[0] = aux[0];
			break;
		case 1:						//DSP2
			Digito_Display_E3[5] = aux[2];
			Digito_Display_E3[4] = aux[1];
			Digito_Display_E3[3] = aux[0];
			break;
	}
}

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t BufferTeclado_E3;	// Buffer del teclado MATRICIAL

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Tecla_E3 ( void )
	\brief Primitiva del teclado de la expansión 3.
	\details Lee el buffer, lo escribe y devuelve el valor leído.
	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
	\date 2014.11.01
	\param void
	\return void
*/

unsigned char Tecla_E3 ( void )
{
	unsigned char Tecla;

	Tecla = BufferTeclado_E3 ;						// Lee
	BufferTeclado_E3 = NO_KEY ;						// Borra tecla

	return Tecla;
}
