/**
	\file PR-TIMER.c
	\brief Primitiva de la máquina de timers.
	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile timers_t Timers[ C_TIMERS ];

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void TimerStart ( EVENT e, TIME t , UNIT u )
	\brief Dispara un timer, según un tiempo.
	\author Catedra Info II.
	\date 2014.11.01
	\param e : Tipo EVENT, número de timer.
	\param t : Tipo TIME, tiempo del timer.
	\param u : Tipo UNIT, unidad de tiempo.
	\return void
*/

void TimerStart( EVENT e, TIME t , UNIT u )
{
	// Bloquear int
	if ( !t )
		Timers[ e ].Event = 1;
	else
		Timers[ e ].Event = 0;

	Timers[ e ].Time = t;
	Timers[ e ].Unit = u;
	// DesBloquear int
	return;
}


// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void TimerStop ( EVENT e )
	\brief Función que detiene un timer.
	\author Catedra Info II.
	\date 2014.11.01
	\param e : Tipo EVENT, número del timer a detener.
	\return void
*/

void TimerStop( EVENT e )
{
	// Bloquear int
	Timers[ e ].Event = 0;
	Timers[ e ].Time = 0;
	Timers[ e ].Unit = 0;
	// Bloquear int
	return;
}


// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void TimerClose ( void )
	\brief Función que detiene todos los timers.
	\author Catedra Info II.
	\date 2014.11.01
	\param void
	\return void
*/

void TimerClose ( void )
{
	int i;

	for ( i = 0 ; i < C_TIMERS ; i ++ )
		TimerStop( i );
}
