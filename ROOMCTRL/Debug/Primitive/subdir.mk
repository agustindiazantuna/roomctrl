################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Primitive/PR-DHT11.c \
../Primitive/PR-EXP.c \
../Primitive/PR-HCSR04.c \
../Primitive/PR-LCD.c \
../Primitive/PR-TIMER.c \
../Primitive/PR-UART0.c \
../Primitive/PR-UART1.c 

OBJS += \
./Primitive/PR-DHT11.o \
./Primitive/PR-EXP.o \
./Primitive/PR-HCSR04.o \
./Primitive/PR-LCD.o \
./Primitive/PR-TIMER.o \
./Primitive/PR-UART0.o \
./Primitive/PR-UART1.o 

C_DEPS += \
./Primitive/PR-DHT11.d \
./Primitive/PR-EXP.d \
./Primitive/PR-HCSR04.d \
./Primitive/PR-LCD.d \
./Primitive/PR-TIMER.d \
./Primitive/PR-UART0.d \
./Primitive/PR-UART1.d 


# Each subdirectory must supply rules for building sources it contributes
Primitive/%.o: ../Primitive/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Application" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Firmware" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Header" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Initialization" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Primitive" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


