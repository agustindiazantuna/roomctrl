################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Firmware/FW-DHT11.c \
../Firmware/FW-EXP.c \
../Firmware/FW-HCSR04.c \
../Firmware/FW-KIT.c \
../Firmware/FW-LCD\ .c \
../Firmware/FW-LPC_TIMER.c \
../Firmware/FW-Systick.c \
../Firmware/FW-TIMER.c \
../Firmware/FW-UART0.c \
../Firmware/FW-UART1.c \
../Firmware/Oscilador.c \
../Firmware/cr_startup_lpc175x_6x.c 

OBJS += \
./Firmware/FW-DHT11.o \
./Firmware/FW-EXP.o \
./Firmware/FW-HCSR04.o \
./Firmware/FW-KIT.o \
./Firmware/FW-LCD\ .o \
./Firmware/FW-LPC_TIMER.o \
./Firmware/FW-Systick.o \
./Firmware/FW-TIMER.o \
./Firmware/FW-UART0.o \
./Firmware/FW-UART1.o \
./Firmware/Oscilador.o \
./Firmware/cr_startup_lpc175x_6x.o 

C_DEPS += \
./Firmware/FW-DHT11.d \
./Firmware/FW-EXP.d \
./Firmware/FW-HCSR04.d \
./Firmware/FW-KIT.d \
./Firmware/FW-LCD\ .d \
./Firmware/FW-LPC_TIMER.d \
./Firmware/FW-Systick.d \
./Firmware/FW-TIMER.d \
./Firmware/FW-UART0.d \
./Firmware/FW-UART1.d \
./Firmware/Oscilador.d \
./Firmware/cr_startup_lpc175x_6x.d 


# Each subdirectory must supply rules for building sources it contributes
Firmware/%.o: ../Firmware/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Application" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Firmware" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Header" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Initialization" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Primitive" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Firmware/FW-LCD\ .o: ../Firmware/FW-LCD\ .c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Application" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Firmware" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Header" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Initialization" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Primitive" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"Firmware/FW-LCD .d" -MT"Firmware/FW-LCD\ .d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


