################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/APP-DHT11.c \
../Application/APP-DOOR.c \
../Application/APP-HCSR04.c \
../Application/APP-RDM6300.c \
../Application/APP-TIMER.c \
../Application/APP-UART1.c \
../Application/RoomCTRL.c \
../Application/crp.c \
../Application/main.c 

OBJS += \
./Application/APP-DHT11.o \
./Application/APP-DOOR.o \
./Application/APP-HCSR04.o \
./Application/APP-RDM6300.o \
./Application/APP-TIMER.o \
./Application/APP-UART1.o \
./Application/RoomCTRL.o \
./Application/crp.o \
./Application/main.o 

C_DEPS += \
./Application/APP-DHT11.d \
./Application/APP-DOOR.d \
./Application/APP-HCSR04.d \
./Application/APP-RDM6300.d \
./Application/APP-TIMER.d \
./Application/APP-UART1.d \
./Application/RoomCTRL.d \
./Application/crp.d \
./Application/main.d 


# Each subdirectory must supply rules for building sources it contributes
Application/%.o: ../Application/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Application" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Firmware" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Header" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Initialization" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Primitive" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


