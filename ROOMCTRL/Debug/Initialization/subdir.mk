################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Initialization/INIT-CONFIG.c \
../Initialization/INIT-DHT11.c \
../Initialization/INIT-EXP.c \
../Initialization/INIT-GPIO.c \
../Initialization/INIT-HCSR04.c \
../Initialization/INIT-KIT.c \
../Initialization/INIT-LCD.c \
../Initialization/INIT-LPC_TIMER.c \
../Initialization/INIT-STATE.c \
../Initialization/INIT-SYSTICK.c \
../Initialization/INIT-UART0.c \
../Initialization/INIT-UART1.c \
../Initialization/INIT.c 

OBJS += \
./Initialization/INIT-CONFIG.o \
./Initialization/INIT-DHT11.o \
./Initialization/INIT-EXP.o \
./Initialization/INIT-GPIO.o \
./Initialization/INIT-HCSR04.o \
./Initialization/INIT-KIT.o \
./Initialization/INIT-LCD.o \
./Initialization/INIT-LPC_TIMER.o \
./Initialization/INIT-STATE.o \
./Initialization/INIT-SYSTICK.o \
./Initialization/INIT-UART0.o \
./Initialization/INIT-UART1.o \
./Initialization/INIT.o 

C_DEPS += \
./Initialization/INIT-CONFIG.d \
./Initialization/INIT-DHT11.d \
./Initialization/INIT-EXP.d \
./Initialization/INIT-GPIO.d \
./Initialization/INIT-HCSR04.d \
./Initialization/INIT-KIT.d \
./Initialization/INIT-LCD.d \
./Initialization/INIT-LPC_TIMER.d \
./Initialization/INIT-STATE.d \
./Initialization/INIT-SYSTICK.d \
./Initialization/INIT-UART0.d \
./Initialization/INIT-UART1.d \
./Initialization/INIT.d 


# Each subdirectory must supply rules for building sources it contributes
Initialization/%.o: ../Initialization/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Application" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Firmware" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Header" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Initialization" -I"/home/bicho/Documentos/INFO2 - A - 2014/RoomCTRL/Source/ROOMCTRL/Primitive" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


