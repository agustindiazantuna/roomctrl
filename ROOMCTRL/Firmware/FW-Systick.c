 /**
 	\file FW-Systick.c
 	\brief Archivo para la interrupción del Systick.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	// DISPLAY
	extern unsigned char Digito_Display_E2[];
	// UART1
	extern volatile uint8_t TX_ENVIANDO;
	// LCD
	extern volatile uint32_t Demora_LCD;
	// DHT11
	extern volatile uint32_t dht_start;
	extern volatile uint8_t dht_cont;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void SysTick_Handler ( void )
	\brief Función handler del systick.
 	\details Contiene las funciones manejadas por polling.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void SysTick_Handler(void)
{
	// EXP3
	BarridoDisplay_E3 ( );
	DriverTeclado_E3 ( );

	// LCD
	Dato_LCD();
	if ( Demora_LCD )
		Demora_LCD--;

	// HC-SR04
	HCSR04_Fw ( );

	// DHT
	if( dht_start > 0 )						// Demora en la inicialización.
		dht_start --;
	if( dht_start == 0 )					// Driver del sensor.
		DHT_Fw ( );
	if( dht_cont > 0 )						// Contador del driver.
		dht_cont++;

	// DOOR
	Relay ( );

	// TIMERS
	Driver_Timers ( );
}
