 /**
 	\file FW-DHT11.c
 	\brief Archivo que contiene el driver del sensor DHT11.
 	\details Contiene la función DHT_Fw().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************



// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void DHT_Fw ( void )
	\brief Driver del sensor DHT11.
 	\details Máquina de estados. IDLE: Coloca un estado bajo en el pin de comunicación. START: Tras esperar el tiempo mínimo del pulso en estado bajo, lo coloca en estado alto, cambia su configuración a captura y habilita las interrupciones. READING: Aguarda a que se termine de leer la trama de datos y configura el pin para la próxima lectura.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void DHT_Fw ( void )
{
	volatile uint8_t ESTADOS_FW_DHT11 = IDLE;
	uint32_t aux = 0;
	switch ( ESTADOS_FW_DHT11 )
	{
		case IDLE :									// Estado IDLE, no hace nada si la variable dht_on vale 0.
			if ( dht_on == 1 )
			{
				ESTADOS_FW_DHT11 = START;
				dht_on = 0;

				SetPIN ( DHT , BAJO );

				dht_cont = 1;
			}
			break;
		case START :
			if ( dht_cont == 10 )
			{
				ESTADOS_FW_DHT11 = READING;
				dht_cont = 1;

				SetPIN ( DHT , ALTO );

				T3CCR = 0x38;						// Habilita int de Captura para CAP3.1 por falling edge y rising edge

				SetDIR ( DHT , ENTRADA );
				SetPINSEL ( DHT , FUNCION_3 );
			}
			break;
		case READING :
			if ( dht_reciving >= 41 )				// SI SE RECIBIERON LOS 41 BITS
			{
				ESTADOS_FW_DHT11 = IDLE;

				BufferDHT_Tem = BufferDHT_Gral;
				BufferDHT_Tem = ( BufferDHT_Tem >> 8 );

				aux = BufferDHT_Gral;
				BufferDHT_Hum = ( aux >> 16 );
				BufferDHT_Hum = ( BufferDHT_Hum / 256 );

				SetPINSEL ( DHT , FUNCION_GPIO );
				SetDIR ( DHT , SALIDA );
				SetPIN ( DHT , ALTO );

				dht_reciving = 0;

				dht_ok = 1;
			}
			else if ( dht_cont == 10 )
			{
				ESTADOS_FW_DHT11 = IDLE;
				BufferDHT_Gral = 0;
				dht_on = 1;
				dht_cont = 0;
			}
			break;
		default :
			break;
	}
}
