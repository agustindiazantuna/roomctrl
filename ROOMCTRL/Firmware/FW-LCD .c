 /**
 	\file LCD.c
 	\brief Archivo que contiene los drivers del LCD.
 	\details Contiene PushLCD(), PopLCD() y Dato_LCD().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t Buffer_LCD[LCDBUFFER_SIZE];
	extern volatile uint32_t inxInLCD;
	extern volatile uint32_t inxOutLCD;
	extern volatile uint32_t cantidadColaLCD;
	extern volatile uint32_t Demora_LCD;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  unsigned char PushLCD ( uint8_t dato , uint8_t control )
	\brief Función para llenar el buffer del LCD.
 	\details Evalua el estado del buffer, si está lleno no se puede escribir. Carga el buffer con el dato ingresado e incrementa el puntero dentro del mismo.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param dato : Tipo uint8_t, valor a enviar al LCD.
 	\param control : Tipo uint8_t, indica si la palabra enviada es de control o de datos.
	\return unsigned char : Vale -1 cuando el buffer está lleno, por lo tanto no puede escribirse, sino vale 0.
*/

unsigned char PushLCD ( uint8_t dato , uint8_t control )
{
	if (cantidadColaLCD >= LCDBUFFER_SIZE )
		return -1;

	Buffer_LCD [ inxInLCD ] = ( dato >> 4 ) & 0x0f;
	if ( control == LCD_CONTROL )
		Buffer_LCD [ inxInLCD ] |= 0x80;

	inxInLCD ++;

	Buffer_LCD [ inxInLCD ] = dato & 0x0f;
	if ( control == LCD_CONTROL )
		Buffer_LCD [ inxInLCD ] |= 0x80;

	cantidadColaLCD += 2;

	inxInLCD ++;
	inxInLCD %= LCDBUFFER_SIZE;

	return 0;
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  int PopLCD (void)
	\brief Función de lectura del buffer.
 	\details Lee el buffer y decrementa el puntero del mismo.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param void
	\return int : El dato sacado del buffer.
*/

int PopLCD (void)											// LEE EL BUFFER
{
	char dato;

	if ( cantidadColaLCD == 0 )
		return -1;

	dato = Buffer_LCD [ inxOutLCD ] ;

	cantidadColaLCD --;

	inxOutLCD ++;
	inxOutLCD %= LCDBUFFER_SIZE;

	return dato;
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void Dato_LCD ( void )
	\brief Función de escritura del LCD.
 	\details Lee el buffer y configura los pines del LCD según el dato obtenido.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Dato_LCD ( void )
{
	int data;

	if ( (data = PopLCD ()) == -1 )
		return;

	SetPIN(LCD_D7,((uint8_t)data) >> 3 & 0x01);
	SetPIN(LCD_D6,((uint8_t)data) >> 2 & 0x01);
	SetPIN(LCD_D5,((uint8_t)data) >> 1 & 0x01);
	SetPIN(LCD_D4,((uint8_t)data) >> 0 & 0x01);

	if( ((uint8_t) data ) & 0x80 )
		SetPIN(LCD_RS,OFF);
	else
		SetPIN(LCD_RS,ON);

	SetPIN( LCD_E , ON );
	SetPIN( LCD_E , OFF );
}
