/*
 * FW-TIMER.c
 *
 *  Created on: Oct 30, 2014
 *      Author: neonlion
 */

 /**
 	\file FW-TIMER.c
 	\brief Drivers de la máquina de timers utilizada.
 	\details El archivo contiene la función AnalizarTimers() y Driver_Timers().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile timers_t Timers[ C_TIMERS ];

	extern volatile uint8_t	decimas;
	extern volatile uint8_t segundos;
	extern volatile uint8_t minutos;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void AnalizarTimers ( UNIT u )
	\brief Analiza el estado del timer.
	\details Coloca un 1 en el campo Event del timer que ha finalizado.
	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
	\date 2014.11.01
	\param void
	\return void
*/

void AnalizarTimers ( UNIT u )
{
	int i;

	for ( i = 0 ; i < C_TIMERS ; i ++ )
	{
		if ( Timers[ i ].Unit == u )
		{
			if ( Timers[ i ].Time )
			{
				Timers[ i ].Time --;
				if ( ! Timers[ i ].Time )
					Timers[ i ].Event = 1;
			}
		}
	}
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Driver_Timers (void)
	\brief Driver de los timers.
 	\details Maneja 3 contadores, que se van decrementando con cada ciclo del Systick. Indica el paso del tiempo, en este caso cada vez que pasa una décima, un segundo y un minuto. Utilizando la relación temporal: Un ciclo del Systick = 2.5 milisegundos, 40 ciclos del Systick = 1 decima, 10 decimas = 1 segundo, 60 segundos = 1 minuto.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Driver_Timers (void)
{
	decimas-- ;

 	if( !decimas )
	{
		decimas = DECIMAS ;

		AnalizarTimers( DECIMAS ) ;

		segundos-- ;

		if( !segundos )
		{
			segundos = SEGUNDOS ;
			AnalizarTimers( SEGUNDOS ) ;

			minutos-- ;
			if( !minutos )
			{
				minutos = MINUTOS ;
				AnalizarTimers( MINUTOS ) ;
			}
		}
	}
}
