 /**
 	\file FW-UART0.c
 	\brief Archivo que contiene los drivers de la UART0.
 	\details Contiene las funciones PopTx0(), PushRx0() y el handler de la interrumpción.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile unsigned char BufferTx0[TOPE_BUFFER_TX0];		//!< Buffer de Transmision
	extern volatile unsigned char BufferRx0[TOPE_BUFFER_RX0];		//!< Buffer de Recepcion
	extern volatile unsigned char IndiceTxIn0,IndiceTxOut0;		//!< Indices de Transmision
	extern volatile unsigned char IndiceRxIn0,IndiceRxOut0;		//!< Indices de Recepcion
	extern volatile char TxStart0;								//!< flag de fin de TX
	extern volatile uint8_t	BufferSalidas0;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn int PopTx0 ( void )
	\brief Driver de transmición de la UART0.
	\details Si los punteros del buffer circular son distintos, hay dato a transmitir, la función devuelve ese dato e incrementa el puntero.
	\author Catedra Info II.
	\date 2014.11.01
	\param void
	\return Devuelve el dato a transmitir.
*/

int PopTx0( void )
{
	int dato = -1;

	if ( IndiceTxIn0!= IndiceTxOut0)//IndiceRxIn1,IndiceRxOut1;
	{
		dato = (int)BufferTx0[IndiceTxOut0];
		IndiceTxOut0 ++;
		IndiceTxOut0 %= TOPE_BUFFER_TX0;
	}

	return dato;
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void PushRx0 ( unsigned char dato )
	\brief Driver de recepción de la UART0.
 	\details Llena el buffer circular de recepción e incrementa el puntero al dato ingresado.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param dato : Tipo unsigned char, dato a ingresar al buffer.
	\return void
*/

void PushRx0(unsigned char dato)
{
	BufferRx0[IndiceRxIn0] = dato;
	IndiceRxIn0 ++;
	IndiceRxIn0 %= TOPE_BUFFER_RX0;
}

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile char TxStart0;									//!< flag de fin de TX

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void UART0_IRQHandler ( void )
	\brief Función handler de la interrumpción de la UART0.
 	\details Evalua el motivo de interrumpción y escribe o lee el bufer de transmición o recepción según el caso correspondiente, vaciando o llenando el buffer.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param void
	\return void
*/

void UART0_IRQHandler ( void )
{
	unsigned char iir;
	unsigned char IntEnCurso , IntPendiente ;
	static int dato ;

	do
	{  //IIR es reset por HW, una vez que lo lei se resetea.

		iir = U0IIR;

		IntPendiente = iir & 0x01;
		IntEnCurso = ( iir >> 1 ) & 0x07;

		switch( IntEnCurso )
		{

			case IIR_RDA:	/* Receive Data Available */
				dato = U0RBR;						// Lectura del dato
				PushRx0( (unsigned char) dato );	// Guardo el dato
				break;

			case IIR_THRE:	/* THRE, transmit holding register empty */
			case 0:
				dato = PopTx0();					// Tomo el dato a Transmitir
				if (  dato >= 0 )
					U0THR = (unsigned char) dato;	// Si hay dato en la cola lo Transmito
				else
					TxStart0 = 0;
				break;

		}
	}
	while( ! ( IntPendiente & 0x01 ) ); /* Me fijo si cuando entró a la ISR habia otra
						     	int. pendiente de atención: b0=1 (ocurre únicamente si dentro del mismo
								espacio temporal lleguan dos interrupciones a la vez) */
 }

