 /**
 	\file FW-HCSR04.c
 	\brief Driver del sensor de ultrasonido.
 	\details Contiene la función HCSR04_Fw().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t hcsr04_on;
	extern volatile uint8_t hcsr04_reciving;
	extern volatile uint8_t hcsr04_ok;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn  void HCSR04_Fw ( void )
	\brief Función driver que controla el sensor HCSR04.
 	\details Máquina de tres estados. IDLE: Cuando la variable de activación está en 1, cambia de estado y pone un estado alto en el pin de TRIGER. START: Espera el tiempo mínimo con el estado alto del TRIGER, luego cambia de estado y coloca un estado bajo. READING: Aguarda a la confirmación de la recepción del pulso y confirma el dato leído.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void HCSR04_Fw ( void )
{
	static uint8_t HCSR04_CONT = 0, ESTADOS_FW_HCSR04 = IDLE;
	switch ( ESTADOS_FW_HCSR04 )
	{
		case IDLE :
			if ( hcsr04_on == 1 )
			{
				ESTADOS_FW_HCSR04 = START;
				SetPIN ( HCSR04_TRIG , ALTO );
			}
			break;
		case START :
			HCSR04_CONT ++;
			if ( HCSR04_CONT == 2 )
			{
				ESTADOS_FW_HCSR04 = READING;
				SetPIN ( HCSR04_TRIG , BAJO );
			}
			break;
		case READING :
			if ( hcsr04_reciving == 1 )
			{
				ESTADOS_FW_HCSR04 = IDLE;
				hcsr04_reciving = 0;
				hcsr04_ok = 1;
			}
			break;
		default :
			break;
	}
}
