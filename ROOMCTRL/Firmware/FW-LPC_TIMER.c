 /**
 	\file FW-LPC_TIMER.c
 	\brief Firmware de las interrupciones de los timers 0 y 3.
 	\details Timer 0 usado en el sensor HCSR04 y Timer 3 en el sensor DHT11.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint32_t len;
	extern volatile uint8_t hcsr04_reciving;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void TIMER0_IRQHandler ( void )
	\brief Handler de interrupción del Timer0.
 	\details Lee el pulso recibido por el sensor HCSR04.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void TIMER0_IRQHandler(void)
{
	volatile uint8_t rising_edge = 1;
	if( ( T0IR & ( 1 << IRCR0 ) ) && rising_edge )	// Si interrumpio CAPTURE
	{
		T0TCR = 0x01;						// Enciendo el temporizador
		T0IR = ( 1 << IRCR0 );				// Borro flag del CAPTURE
		rising_edge = 0;
	}

	if( ( T0IR & ( 1 << IRCR0 ) ) && ~rising_edge )// Si interrumpio CAPTURE
	{
		T0IR = ( 1 << IRCR0 ); 				// Borro flag del CAPTURE

		len = ( T0CR0 * 4 ) / 100;			// Convierto el tiempo a microsegundos
		len = len / 58;						// Usando la fórmula de la hoja de datos lo convierto a centímetros

		T0TCR = 0x00;						// Apago el temporizador
		T0TCR = 0x02;						// Reseteo el temporizador
		rising_edge = 1;

		hcsr04_reciving = 1;
	}
}

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t dht_reciving;
	extern volatile uint32_t BufferDHT_Gral;
	extern volatile uint8_t BufferDHT_Par;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void TIMER3_IRQHandler ( void )
	\brief Función principal del proyecto.
 	\details Contiene las funciones de inicialización del hardware y de control del mismo.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void TIMER3_IRQHandler ( void )
{
    if( ( T3IR & ( 1 << IRCR1 ) ) )			// Si está activo el flag de CAPTURE
    {
		T3IR = ( 1 << IRCR1 );				// Borro flag del CAPTURE

		if ( GetPIN ( DHT , ALTO ) )		// Si fue por rising edge
		{
			T3TCR |= (0x01 << 1);			// Reseteo el temporizador
			T3TCR &= ~(0x01 << 1);			// Saco reset del temporizador
			T3TCR |= 0x01;					// Enciendo el temporizador
		}

		else
		{
			if ( dht_reciving > 1 )
			{
				int t = ( T3CR1 * 4 ) / 100;

				if( dht_reciving < 34 )
					if ( t > 50 )
						BufferDHT_Gral |= ( 0x01 << ( 33 - dht_reciving ) );

				if ( dht_reciving > 42 )
						T3CCR = 0x00;					// Deshabilita interrupciones.
			}
			else
				BufferDHT_Gral = 0;

			dht_reciving ++;

			T3TCR &= ~(0x01 << 0);						// Apago el temporizador.
			T3TCR |= (0x01 << 1);						// Reseteo el temporizador.
			T3TCR &= ~(0x01 << 1);						// Saco reset del temporizador.
		}
    }
}
