 /**
 	\file FW-UART1.c
 	\brief Archivo que contiene los drivers de la UART1.
 	\details Contiene las funciones PopTx(), PushRx() y el handler de la interrumpción.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile unsigned char BufferTx[TOPE_BUFFER_TX];		//!< Buffer de Transmision
	extern volatile unsigned char BufferRx[TOPE_BUFFER_RX];		//!< Buffer de Recepcion
	extern volatile unsigned char IndiceTxIn,IndiceTxOut;		//!< Indices de Transmision
	extern volatile unsigned char IndiceRxIn,IndiceRxOut;		//!< Indices de Recepcion
	extern volatile char TxStart;								//!< flag de fin de TX
	extern volatile uint8_t	BufferSalidas;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn int PopTx ( void )
	\brief Driver de transmición de la UART0.
 	\details Si los punteros del buffer circular son distintos, hay dato a transmitir, la función devuelve ese dato e incrementa el puntero.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param void
	\return Devuelve el dato a transmitir.
*/

int PopTx( void )
{
	int dato = -1;

	if ( IndiceTxIn != IndiceTxOut )
	{
		dato = ( unsigned int ) BufferTx[IndiceTxOut];
		IndiceTxOut ++;
		IndiceTxOut %= TOPE_BUFFER_TX;
	}
	return dato;
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void PushRx ( unsigned char dato )
	\brief Driver de recepción de la UART1.
 	\details Llena el buffer circular de recepción e incrementa el puntero al dato ingresado.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param dato : Tipo unsigned char, dato a ingresar al buffer.
	\return void
*/

void PushRx ( unsigned char dato )
{
	BufferRx[IndiceRxIn] = dato;
	IndiceRxIn ++;
	IndiceRxIn %= TOPE_BUFFER_RX;
}

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile char TxStart;									//!< flag de fin de TX

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void UART1_IRQHandler ( void )
	\brief Función handler de la interrumpción de la UART1.
 	\details Evalua el motivo de interrumpción y escribe o lee el bufer de transmición o recepción según el caso correspondiente, vaciando o llenando el buffer.
 	\author Catedra Info II.
 	\date 2014.11.01
 	\param void
	\return void
*/

void UART1_IRQHandler(void)
{
	unsigned char iir,lsr;
	unsigned char IntEnCurso , IntPendiente ;
	int dato ;

	do
	{  //IIR es reset por HW, una vez que lo lei se resetea.
		iir = U1IIR;

		IntPendiente = iir & 0x01;
		IntEnCurso = ( iir >> 1 ) & 0x07;
		IntEnCurso = ( iir >> 1 ) & 0x07;
		IntEnCurso = ( iir >> 1 ) & 0x07;

		switch( IntEnCurso )
		{
			case IIR_RLS:
				lsr = U1LSR;						// Con la lectura se borra la int
				if ( lsr & ( LSR_OE | LSR_PE | LSR_FE | LSR_RXFE|LSR_BI ) )
				{
					dato = U1RBR;					// Leo para borrar el buffer
					return;
				}
				break;
			case IIR_RDA:	// Receive Data Available
				U1LSR &=~ LSR_RDR;					// Borrar flag de Rx
				dato = U1RBR;						// Lectura del dato
				PushRx( (unsigned char ) dato );	// Guardo el dato
				break;

			case IIR_THRE:	// THRE, transmit holding register empty
				U1LSR &= ~LSR_THRE;					// Borrar flag de Tx
				dato = PopTx( );					// Tomo el dato a Transmitir
				if ( dato >= 0 )
					U1THR = (unsigned char) dato;	// si hay dato en la cola lo Transmito
				else
					TxStart = 0;
				break;
		}
	}
	while( ! ( IntPendiente & 0x01 ) ); // Me fijo si cuando entré‚ a la ISR habia otra
						     			// interrumpción pendiente de atenci¢n: b0=1 (ocurre únicamente si dentro del mismo
										// espacio temporal lleguan dos interrupciones a la vez)
}

