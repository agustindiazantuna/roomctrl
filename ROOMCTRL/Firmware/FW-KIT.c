 /**
 	\file FW-KIT.c
 	\brief Archivo que contiene drivers del KIT Infotronic.
 	\details Contiene la función Relay().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile uint8_t relays;
	extern volatile uint8_t led_green;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void Relay ( void )
	\brief Función que prende y apaga los relays.
 	\details La función revisa el buffer relays y en base a su valor prende y apaga el relay que simula el estado de la puerta. También controla la actividad del led.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void Relay ( void )
{
	if ( relays )						// relay = 0 significa que la puerta está abierta.
		SetPIN ( RELAY0 , OFF );
	else if ( !relays )
		SetPIN ( RELAY0, ON );

	if ( led_green )
	{
		SetPIN ( RGBR , OFF );
		SetPIN ( RGBG , ON );
	}
	else
	{
		SetPIN ( RGBR , ON );
		SetPIN ( RGBG , OFF );
	}
}
