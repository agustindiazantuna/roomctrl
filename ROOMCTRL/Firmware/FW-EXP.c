 /**
 	\file FW-EXP.c
 	\brief Archivo con los drivers de la Expansión 3.
 	\details Contiene las funciones BarridoDisplay_E3(), DriverTeclado_E3(), TecladoHW_E3, BounceTeclado_E3().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
*/

// *************************************************************************************************************
// INCLUDES*****************************************************************************************************
// *************************************************************************************************************

	#include "Infotronic.h"

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern uint8_t Digito_Display_E3 [CANT_DIGITOS];

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void BarridoDisplay_E3 ( void )
	\brief Función driver del display de 7 segmentos.
 	\details Barre los dígitos del display de 7 segmentos leyendo el buffer y configurando los pines correspondientes.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void BarridoDisplay_E3 ( void )
{
		static uint8_t digito = 0;
		uint8_t aux = Digito_Display_E3 [digito];
		SetPIN ( BCD_A, ON);
		SetPIN ( BCD_B, ON);
		SetPIN ( BCD_C, ON);
		SetPIN ( BCD_D, ON);
		SetPIN ( seg_DP, OFF);
		if ( ! digito )
		{
			SetPIN ( RST , ON );
			SetPIN ( RST , ON );
			SetPIN ( RST , ON );
			SetPIN ( RST , OFF );
		}
		else
		{
			SetPIN ( CLK , OFF );
			SetPIN ( CLK , OFF );
			SetPIN ( CLK , OFF );
			SetPIN ( CLK , ON );
		}
		SetPIN ( BCD_A , ( aux & (uint8_t) 0x01 ) ) ;
		SetPIN ( BCD_B , (aux >> 1) & (uint8_t) 0x01 );
		SetPIN ( BCD_C , (aux >> 2) & (uint8_t) 0x01 );
		SetPIN ( BCD_D , (aux >> 3) & (uint8_t) 0x01 );

		digito ++;
		digito %= CANT_DIGITOS;
}

// *************************************************************************************************************
// VARIABLES****************************************************************************************************
// *************************************************************************************************************

	extern volatile unsigned char BufferTeclado_E3;

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void DriverTeclado_E3 ( void )
	\brief Función driver del teclado.
 	\details Contiene la función BounceTeclado_E3 ().
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/

void DriverTeclado_E3 ( void )
{
	BounceTeclado_E3 ();
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn unsigned char TecladoHW_E3 ( void )
	\brief Función de lectura del teclado matricial.
 	\details Utilizando el concepto de multiplexado del teclado, leemos el estado de los pines correspondientes a las filas y columnas así sabemos que valor retornar.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return unsigned char : Valor retornado corresponde a la tecla pulsada.
*/

unsigned char TecladoHW_E3 (void)
{
	SetPIN(FILA_0,ON); SetPIN(FILA_1,OFF); SetPIN(FILA_2,OFF); SetPIN(FILA_3,OFF);
	SetPIN(FILA_0,ON); SetPIN(FILA_1,OFF); SetPIN(FILA_2,OFF); SetPIN(FILA_3,OFF);

	if( GetPIN( COL_0 , ALTO ) )		return 3;
	if( GetPIN( COL_1 , ALTO ) )		return 4;

	SetPIN(FILA_0,OFF); SetPIN(FILA_1,ON); SetPIN(FILA_2,OFF); SetPIN(FILA_3,OFF);
	SetPIN(FILA_0,OFF); SetPIN(FILA_1,ON); SetPIN(FILA_2,OFF); SetPIN(FILA_3,OFF);

	if( GetPIN( COL_0 , ALTO ) )		return 0;
	if( GetPIN( COL_1 , ALTO ) )		return 7;

	SetPIN(FILA_0,OFF); SetPIN(FILA_1,OFF); SetPIN(FILA_2,ON); SetPIN(FILA_3,OFF);
	SetPIN(FILA_0,OFF); SetPIN(FILA_1,OFF); SetPIN(FILA_2,ON); SetPIN(FILA_3,OFF);

	if( GetPIN( COL_0 , ALTO ) )		return 2;
	if( GetPIN( COL_1 , ALTO ) )		return 5;

	SetPIN(FILA_0,OFF); SetPIN(FILA_1,OFF); SetPIN(FILA_2,OFF); SetPIN(FILA_3,ON);
	SetPIN(FILA_0,OFF); SetPIN(FILA_1,OFF); SetPIN(FILA_2,OFF); SetPIN(FILA_3,ON);

	if( GetPIN( COL_0 , ALTO ) )		return 1;
	if( GetPIN( COL_1 , ALTO ) )		return 6;

	return NO_KEY;
}

// *************************************************************************************************************
// FUNCTION*****************************************************************************************************
// *************************************************************************************************************

/**
	\fn void BounceTeclado_E3 ( void )
	\brief Función de filtrado del rebote.
 	\details Utilizando la función de lectura del teclado se analiza el rebote, esperando 4 resultados iguales consecutivos, tomando como válida esta variable y guardándola en el buffer.
 	\author Brusa, German. Díaz Antuña, Agustín. Traetta, Lucas.
 	\date 2014.11.01
 	\param void
	\return void
*/
void BounceTeclado_E3 (void)					// Debouncer
{
	static unsigned char CodigoAnterior = NO_KEY;
	static unsigned char EstadosEstables = 0;
	unsigned char CodigoActual;

	CodigoActual = TecladoHW_E3 ( );

	if ( CodigoActual == NO_KEY )						//No se pulso tecla o rebote
	{
			CodigoAnterior = NO_KEY ;
			EstadosEstables = 0 ;
			return ;
	}
	if ( EstadosEstables == 0 )							//Primera vez
	{
			CodigoAnterior = CodigoActual ;
			EstadosEstables = 1;
			return ;
	}
	if ( EstadosEstables == CANT_REBOTES )				//Acepto la tecla
	{
			EstadosEstables = CANT_REBOTES + 1;			//Para que no vuelva a entrar hasta que vuelvan a apretar
			BufferTeclado_E3 = CodigoAnterior ;
			return ;
	}
	if ( EstadosEstables > CANT_REBOTES )
			return ;
	if ( CodigoActual == CodigoAnterior )
			EstadosEstables ++ ;
	else
			EstadosEstables = 0;
	return;
}
